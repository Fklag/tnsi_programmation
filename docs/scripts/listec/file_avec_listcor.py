class File:

    def __init__(self):
        self.valeurs = []

    def enfiler(self, valeur):
        self.valeurs.append(valeur)

    def defiler(self):
        if self.valeurs:
            return self.valeurs.pop(0)

    def estVide(self):
        return self.valeurs == []

    def taille(self):
        return len(self.valeurs)

    def lireAvant(self):
        return self.valeurs[0]

    def __str__(self):
        ch = "\nEtat de la file:\n"
        for x in self.valeurs:
            ch +=  str(x) + " "
        return ch



q = File()
q.enfiler(9)
q.enfiler(2)
q.enfiler(5)

print(q)

q.defiler()
q.enfiler(7)

print("La file est-elle vide: ", q.estVide())

print(q)
print("Longueur de la file:", q.taille())