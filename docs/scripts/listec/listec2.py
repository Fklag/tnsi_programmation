class Maillon:
    def \_\_init\_\_(self):
        self.val = None
        self.suiv = None # Pas de maillon suivant

    def \_\_str\_\_(self):
        if  self.suiv is not None:
            return str(self.val) + "-" + str(self.suiv.val)
        else:
            return str(self.val) + "-" + str(self.suiv)

class ListeC:
    def \_\_init\_\_(self):
       self.tete = None # Liste vide

L = ListeC()
M1, M2 = Maillon(), Maillon()
M1.val, M1.suiv = 1, M2
M2.val, M2.suiv = 2, None
L.tete = M1

def get_maillon_indice(L, i):
   """ Renvoie Maillon d'indice i dans la liste L """


def ajouter_debut(L, nM):
   """ Ajoute le maillon nM en tête de la liste L """


def ajouter_fin(L, nM):
   """ Ajoute le maillon nM en queue de la liste L """
