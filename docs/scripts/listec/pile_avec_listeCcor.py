class Maillon():
    def __init__(self, data = None):
        self.data = data
        self.suiv = None # adresse (en Python, on pointe directement vers un Maillon)
    def __repr__(self):
        return self.data.__repr__()

class Pile:
    def __init__(self):
        self.top = None


    def empiler(self, valeur):
        M = Maillon(valeur)
        M.suiv = self.top
        self.top = M

    def depiler(self):
        if self.estVide():
            raise ValueError("pile vide")
        M = self.top
        self.top = M.suiv
        return M.data

    def estVide(self):
        return self.top is None


    def lireSommet(self):
        return self.top.data

    def taille(self):
        M = self.top
        l = 0
        while M is not None:
            l += 1
            M = M.suiv
        return l

    def __str__(self):
        ch = "\nEtat de la pile:\n"
        sommet = self.top
        while sommet != None:
            ch += "|\t" + str(sommet.data) + "\t|" + "\n"
            sommet = sommet.suiv
        return ch

    # def __repr__(self):
    #     m = self.top
    #     l = []
    #     while m is not None:
    #         l.append(m.__repr__())
    #         m = m.suiv
    #     return " ".join(l)


p = Pile()
p.empiler(9)
p.empiler(2)
p.empiler(5)

print(p)
print(p.lireSommet())
print(p.taille())


p.depiler()
p.depiler()
p.empiler(8)

print(p)
print(p.lireSommet())
print(p.taille())