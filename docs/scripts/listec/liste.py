def vide():
    return None

def cons(x,L):
    return(x,L)

def ajouteEnTete(x,L):
    return cons(x,L)

def supprEnTete(L):
    return L[0],L[1]

def estVide(L):
    return L is None

def compte(L):
    if estVide(L):
        return 0
    return 1 + compte(L[1])

def tete(L):
    return L[0] if L else None

def queue(L):
    return L[1] if L else ()


