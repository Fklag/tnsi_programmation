L, l = ListeC(), ListeC()
M1, M2, m1, m2, m3 = Maillon(), Maillon(), Maillon(), Maillon(), Maillon()
M1.val, M1.suiv = 1, M2
M2.val, M2.suiv = 2, None
L.tete = M1
m1.val, m1.suiv = 1, m2
m2.val, m2.suiv = 2, m3
m3.val, m3.suiv = 3, None
l.tete =  m1

b1 = ['get_maillon_indice(ListeC(),0) == None', 'get_maillon_indice(L,0) == M1', 'get_maillon_indice(L,1) == M2', 'get_maillon_indice(l,0) == m1', 'get_maillon_indice(l,1) == m2', 'get_maillon_indice(l,2) == m3']
b2 = ['ajouter_debut(l, m3) == None', 'get_maillon_indice(l,0) == m3', 'get_maillon_indice(l,1) == m1', 'get_maillon_indice(l,2) == m2', 'get_maillon_indice(l,3) == m3']
b3 = ['ajouter_fin(L, m3) == None', 'get_maillon_indice(L,0) == M1', 'get_maillon_indice(L,1) == M2', 'get_maillon_indice(L,2) == m3']

benchmark = (b1, b2, b3)