class File:

    def __init__(self):
        self.valeurs = []

    def enfiler(self, valeur):
        self.valeurs.append(valeur)

    def defiler(self):
        if self.valeurs:
            return self.valeurs.pop(0)

    def estVide(self):
        return self.valeurs == []

    def taille(self):
        return len(self.valeurs)

    def lireAvant(self):
        return self.valeurs[0]

    def __str__(self):
        ch = "\nEtat de la file:\n"
        for x in self.valeurs:
            ch +=  str(x) + " "
        return ch

class Pile:
    def __init__(self):
        self.valeurs = []

    def empiler(self, valeur):
        self.valeurs.append(valeur)

    def depiler(self):
        if self.valeurs:
            return self.valeurs.pop()

    def estVide(self):
        return self.valeurs == []

    def taille(self):
        return len(self.valeurs)

    def lireSommet(self):
        return self.valeurs[-1]


    def __str__(self):
        ch = ''
        for x in self.valeurs:
            ch = "|\t" + str(x) + "\t|" + "\n" + ch
        ch = "\nEtat de la pile:\n" + ch
        return ch

def inverse_pile(p):
    """
    :entrée/sortie p: Tpile
    :post-cond: inverse l'ordre des éléments de p
    """
    f = File()
    while not p.estVide() :
        e = p.lireSommet()
        p.depiler()
        f.enfiler(e)
    while not f.estVide():
        e = f.lireAvant()
        f.defiler()
        p.empiler(e)

p = Pile()
p.empiler(9)
p.empiler(2)
p.empiler(5)

print(p)
inverse_pile(p)
print(p)

