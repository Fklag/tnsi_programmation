# Interface et implémentation

![BO1](assets/images/BO1.png){: .center .width=50% }

!!! info "Distinguer interfaçage et implémentation"
    Cette année, nous allons voir de nouvelles façons d'organiser et de traiter les données, ce que l'on appelle des **structures de données** (*implémentation explicite d’un ensemble organisé d’objets, avec la réalisation des opérations d’accès, de construction et de modification*).  

???+ warning "On rencontrera, ... :"
    - des structures **linéaires** (*les éléments sont ordonnés*) comme la **liste**, la **pile** et la **file**;
    - des structures **séquentielles** comme les **tableaux** et les **dictionnaires**;  
      (*les éléments ont une unique place numérotée, appelée indice ou index*)
    - des structures **relationnelles** telles que les **arbres** ou les **graphes**. 
    
Dans ce chapitre, nous allons commencer par distinguer la structure de données de son implémentation.<!-- en s'appuyant sur les tableaux et dictionnaires vus en première.-->

=== "L'interfaçage"
    !!! warning "L'interfaçage permet de spécifier la représentation d'une structure de données qui ne serait pas présente nativement et que l'on souhaite utiliser pour un programme."  
        Il s'agit de **décrire de façon formelle** les **opérations** et **propriétés** que les données doivent respecter  
        (on peut alors utiliser ces données dans un *environnement théorique de développement* en s'affranchissant du langage)


    !!! tip "types abstraits de données *TAD*"
    
        Dans l'interfaçage, on réfléchit à une **notation** pour représenter des données, puis les **opérations** que l'on pourra appliquer et les propriétés de ces différentes opérations.
        On parle alors d'un **type de données abstrait**.
    
        Les différentes opérations de l'interfaçage s'appelle les **primitives**  
        et les propriétés que les opérations doivent respecter forment ce qu'on appelle la **sémantique**.
    
    === "Le type `bool` pour comprendre"
        !!! example "Spécification du le type `bool`"
            - **Notation :** Vrai, Faux (par exemple).
            - **Primitives :** Opérateurs OU, ET, NON etc...
            - **Sémantique :** Sens habituel pour les opérateurs logiques.

    === "Création d'un type de données entiers relatifs"
        !!! example "Création d'un type de données entiers relatifs"
            - **Notation :** séquence de chiffres précédés d'un signe $+$ ou $-$ ;
            - **Primitives :** opérations arithmétiques de base $+$, $-$, $\times$ et $\div$ ;
            - **Sémantique :** propriétés habituelles de ces opérations ;
            - On ne se soucie pas de la représentation en binaire de ces nombres.

=== "L'implémentation"
    !!! warning "L'implémentation consiste à choisir une représentation en fonction du langage de programmation choisi."
        Un type de données abstrait (TAD) peut ainsi avoir différentes implémentations, en `Python` ou en langage `C` par exemple.

        En langage `Python` des types de données simples (`#!python int`, `#!python float`, `#!python bool`, `#!python str`) ou construits (`#!python list`, `#!python tuple`, `#!python dict`) sont **implémentés**, il n'est pas besoin de savoir commment ces types sont représentés dans la machine pour pouvoir les utiliser.

        Par la suite, nous allons **implémenter les types abstraits de données** sous différentes structures de données concrètement en machine :

        - un **algorithme** manipule des types abstraits de données (c'est la phase de conception théorique)
        - un **programme** manipule des structure de données concrètes (c'est la phase de réalisation pratique)


!!! example "Exemple - Implémenter la structure de données linéaires de type `#!python liste`"

=== "Spécifier une structure de données par son interfaçage"
    !!! warning "On veut construire une séquence ordonnée d'éléments qui ont un même type (entier, chaîne de caractères, etc.)"

        - Si on connaît un élément, on doit pouvoir connaître le suivant et donc disposer d'une fonction qui permette d'accéder au successeur.
        
        On vient de construire une première **primitive** (opération obligatoire) pour spécifier l'interfaçage de cette structure de données de type `#!python liste`.

        On peut ajouter d'autres primitives pour compléter l'interfaçage de cette structure de données :

        - Création d'une liste vide : `#!python creer()`  
        - Ajouter un élément `a` au début de `L` : `#!python ajout_debut(a,L)`  
        - Ajouter un élément `a` à la fin de `L` : `#!python ajout_fin(a,L)`  
        - Supprimer le premier élément de la liste `L` que l'on appelle "en-tête" : `#!python supp(L)`  
        - Obtenir l'en-tête de la liste `L` : `#!python entete(L)`  
        - Obtenir la longueur de la liste `L` : `#!python longueur(L)` 

        Ces **primitives** permettent de programmer dans un environnement théorique de développement sans s'occuper de l'implémentation.

        ![liste](assets/images/interface-liste-1.png)

=== "Écrire plusieurs implémentations d'une même structure de données - Exemple en `Python`"
    !!! warning "Une fois l'interfaçage de la `liste` réalisé, on peut l'implémenter dans un langage de programmation."

        **Implémentation native en `Python`**  

        | `Python`|  Explication  |
        |:--------|:--------------|
        | `#!python L = []`         | On crée la liste vide `L` |
        | `#!python L.append(x)`    | On ajoute `x` à la fin de la liste `L` |
        | `#!python len(L)`         | On obtient la longueur de la liste `L`  |

        **Implémentation en `Python`**  

        <style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:0px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-c3ow">`Python`</th>
    <th class="tg-c3ow">Explication </th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky"><pre>
    def creer():
        return None
    </pre></td>
    <td class="tg-c3ow">On crée une liste vide</td>
  </tr>
  <tr>
    <td class="tg-0pky"><pre>
    def ajout_debut(a,L):
        return (a,L)
    </pre></td>
    <td class="tg-c3ow">On ajoute au début de la liste l'élément `a`</td>
  </tr>
  <tr>
    <td class="tg-0pky"><pre>
    def ajout_fin(a,L):
        return (L,a)
    <pre></td>
    <td class="tg-c3ow">On ajoute à la fin de la liste l'élément `a`</td>
  </tr>
</tbody>
</table>
<!--        | `Python`|  Explication  |
        |:--------|:--------------|
        | ```py3
            def creer():
                return None
          ```    | On crée une liste vide |
        | ```py3
            def ajout_debut(a,L):
                return (a,L)
          ```    | On ajoute au début de la liste l'élément `a` |
        | ```py3
            def ajout_fin(a,L):
                return (L,a)
          ```    | On ajoute à la fin de la liste l'élément `a` |
-->
