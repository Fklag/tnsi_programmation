# Paradigme de Programmation [^1]

Les langages de programmation permettent de traiter des problèmes selon différentes approches.

![paradigme](images/programmparadigme.png){: .center width=75%}[^2]

## Programmation impérative [^3]
**La programmation impérative** est un paradigme de programmation qui décrit les opérations en séquences d'instructions  
exécutées par l'ordinateur pour modifier l'état du programme.   
Ce type de programmation est le plus répandu parmi l'ensemble des langages de programmation existants.     
C, Pascal ou encore les interpréteurs de commandes Unix sont des langages impératifs.

## Les langages déclaratifs
Les **langages déclaratifs** permettent d'écrire la spécification du problème et laissent l'implémentation du langage trouver une façon 
efficace de réaliser les calculs nécessaires à sa résolution.   
SQL est un langage déclaratif que vous êtes susceptible de connaître.  
Une requête SQL décrit le jeu de données que vous souhaitez récupérer et le moteur SQL choisit la méthode de résolution 
(parcourir les tables ou utiliser les index, l'ordre de résolution des sous-clauses, etc).

## La programmation orientée objet

Dans la programmation orientée objet le programme est divisé en parties appelées objets.  
La programmation orientée objet est un concept de programmation qui se concentre sur l'objet plutôt que sur les actions et les données plutôt que sur la logique.

**Les programmes orientés objet** manipulent des ensembles d'objets. Ceux-ci possèdent un état interne et des méthodes qui interrogent ou 
modifient cet état d'une façon ou d'une autre.  

![poo](images/ProgrammationOrienteeObjet.png){: .center .imagepng}

Un objet est donc une entité avec une identité et un état, il peut répondre aux **messages** qui lui sont envoyés en appelant l'une de ses méthodes qui correspond au message reçu.  
L'idée est que l'état est encapsulé dans l'objet et ne peut être manipulé qu'en envoyant des messages à l'objet (également appelé «méthodes d'appel d'un objet»).  
Dans une méthode, vous avez accès à l'état de l'objet, généralement via une liaison de variable spéciale appelée « this » ou « self ». Le principe de cacher l'état de l'objet s'appelle « Encapsulation ».

Smalltalk et Java sont deux langages orientés objet.  
C++ et Python gèrent la programmation orientée objet mais n'imposent pas l'utilisation de telles fonctionnalités.

## La programmation procédurale

Dans la **programmation procédurale**, les programmes sont basés sur des fonctions, et les données peuvent être facilement accessibles et modifiables

![procedurale](images/ProgrammationProcedurale.png){: .center .imagepng}

Dans la programmation procédurale le programme est divisé en petites parties appelées procédures ou fonctions.
Comme son nom l'indique, la programmation procédurale contient une procédure étape par étape à exécuter.  
Ici, les problèmes sont décomposés en petites parties et ensuite, pour résoudre chaque partie, une ou plusieurs fonctions sont utilisées.

Dans l'idéal, les fonctions produisent des sorties à partir d'entrées et ne possède pas d'état interne qui soit susceptible de modifier 
la sortie pour une entrée donnée (**pas d'effets de bord**). 

Une **procédure**, aussi appelée routine, sous-routine ou fonction (*à ne pas confondre avec les fonctions de la programmation fonctionnelle reposant sur des fonctions mathématiques*), contient simplement une série d'étapes à réaliser.  
N'importe quelle procédure peut être appelée à n'importe quelle étape de l'exécution du programme, y compris à l'intérieur d'autres procédures, voire dans la procédure elle-même (*récursivité*). 

La programmation procédurale est un meilleur choix qu'une simple programmation séquentielle.  
Les avantages sont en effet les suivants :

- la possibilité de réutiliser le même code à différents emplacements dans le programme sans avoir à le retaper (factorisation), ce qui a pour effet la réduction de la taille du code source et un gain en localité des modifications, donc une amélioration de la maintenabilité (compréhension plus rapide, réduction du risque de régression) ;
- une façon plus simple de suivre l'exécution du programme : la programmation procédurale permet de se passer d'instructions telles que `goto`, évitant ainsi bien souvent de se retrouver avec un programme compliqué qui part dans toutes les directions ; cependant, la programmation procédurale permet les « effets de bord », c'est-à-dire la possibilité pour une procédure qui prend des arguments de modifier des variables extérieures à la procédure auxquelles elle a accès (variables de contexte plus global que la procédure).

??? "Table de comparaison"
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;border-style:1 px solid}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-cv16{font-weight:bold;background-color:#dae8fc;border-color:inherit;text-align:center}
    .tg .tg-xldj{border-color:inherit;text-align:left}
    </style>
    
    <table class="tg">
    <tr>
    <th class="tg-cv16"> </th>
        <th class="tg-cv16">Programmation Procédurale</th>
        <th class="tg-cv16">Programmation Orientée Objet</th>
    </tr>
    <tr>
        <td class="tg-xldj">Programmes</td>
        <td class="tg-xldj">Le programme principal est divisé en petites parties selon les fonctions.</td>
        <td class="tg-xldj">Le programme principal est divisé en petit objet en fonction du problème.</td>
    </tr>
    <tr>
        <td class="tg-xldj">Les données</td>
        <td class="tg-xldj">Le programme principal est divisé en petites parties selon les fonctions.</td>
        <td class="tg-xldj">Le programme principal est divisé en petit objet en fonction du problème.</td>
    </tr>
    <tr>
        <td class="tg-xldj">Programmes</td>
        <td class="tg-xldj">Chaque fonction contient des données différentes.</td>
        <td class="tg-xldj">Les données et les fonctions de chaque objet individuel agissent comme une seule unité.</td>
    </tr>
    <tr>
        <td class="tg-xldj">Permission</td>
        <td class="tg-xldj">Pour ajouter de nouvelles données au programme, l'utilisateur doit s'assurer que la fonction le permet.</td>
        <td class="tg-xldj">Le passage de message garantit l'autorisation d'accéder au membre d'un objet à partir d'un autre objet.</td>
    </tr>
    <tr>
        <td class="tg-xldj">Exemples</td>
        <td class="tg-xldj">Pascal, Fortran.</td>
        <td class="tg-xldj">PHP5, C ++, Java.</td>
    </tr>
    <tr>
        <td class="tg-xldj">Accès</td>
        <td class="tg-xldj">Aucun spécificateur d'accès n'est utilisé.</td>
        <td class="tg-xldj">Les spécificateurs d'accès public, private, et protected sont utilisés.</td>
    </tr>
    <tr>
        <td class="tg-xldj">La communication</td>
        <td class="tg-xldj">Les fonctions communiquent avec d'autres fonctions en gardant les règles habituelles.</td>
        <td class="tg-xldj">Les objets communiquent entre eux via des messages.</td>
    </tr>
    <tr>
        <td class="tg-xldj">Contrôle des données</td>
        <td class="tg-xldj">La plupart des fonctions utilisent des données globales.</td>
        <td class="tg-xldj">Chaque objet contrôle ses propres données.</td>
    </tr>
    <tr>
        <td class="tg-xldj">Importance</td>
        <td class="tg-xldj">Les fonctions ou les algorithmes ont plus d'importance que les données dans le programme.</td>
        <td class="tg-xldj">Les données prennent plus d'importance que les fonctions du programme.</td>
    </tr>
    <tr>
        <td class="tg-xldj">Masquage des données</td>
        <td class="tg-xldj">Il n'y a pas de moyen idéal pour masquer les données.</td>
        <td class="tg-xldj">Le masquage des données est possible, ce qui empêche l'accès illégal de la fonction depuis l'extérieur.</td>
    </tr>
    </table>

    <!-- 
    <table class="tg">
    <tr>
    <th class="tg-cv16"> </th>
        <th class="tg-cv16">Programmation fonctionnelle</th>
        <th class="tg-cv16">Programmation Orientée Objet</th>
    </tr>
    <tr>
        <td class="tg-xldj">Définition</td>
        <td class="tg-xldj">La programmation fonctionnelle met l'accent sur l'évaluation des fonctions.</td>
        <td class="tg-xldj">Programmation orientée objet basée sur un concept d'objets.</td>
    </tr>
    <tr>
        <td class="tg-xldj">Les données</td>
        <td class="tg-xldj">La programmation fonctionnelle utilise des données non modifiables.</td>
        <td class="tg-xldj">Orienté objet utilise des données modifiables.</td>
    </tr>
    <tr>
        <td class="tg-xldj">Programmation parallèle</td>
        <td class="tg-xldj">Programmation parallèle prise en charge par la programmation fonctionnelle.</td>
        <td class="tg-xldj">La programmation orientée objet ne prend pas en charge la programmation parallèle.</td>
    </tr>
    <tr>
        <td class="tg-xldj">Exécution</td>
        <td class="tg-xldj">En programmation fonctionnelle, les instructions peuvent être exécutées dans n'importe quel ordre.</td>
        <td class="tg-xldj">Dans la POO, les instructions doivent être exécutées dans un ordre particulier.</td>
    </tr>
    <tr>
        <td class="tg-xldj">Itération</td>
        <td class="tg-xldj">En programmation fonctionnelle, la récursivité est utilisée pour les données itératives.</td>
        <td class="tg-xldj">Dans le POO, les boucles sont utilisées pour les données itératives.</td>
    </tr>
    <tr>
        <td class="tg-xldj">Élément</td>
        <td class="tg-xldj">Les éléments de base de la programmation fonctionnelle sont les variables et les fonctions.</td>
        <td class="tg-xldj">Les éléments de base de la programmation orientée objet sont les objets et les méthodes.</td>
    </tr>
    <tr>
        <td class="tg-xldj">Utilisation</td>
        <td class="tg-xldj">La programmation fonctionnelle est utilisée uniquement lorsqu'il y a peu de choses avec plus d'opérations.</td>
        <td class="tg-xldj">La programmation orientée objet est utilisée lorsqu'il y a beaucoup de choses avec peu d'opérations.</td>
    </tr>
    </table>
    -->

[^1]: Source : [docs.python : Guide pratique de la programmation fonctionnelle](https://docs.python.org/fr/3/howto/functional.html)
[^2]: Source : [Paradigmes de programmation : quels sont les principes de la programmation ?](https://www.ionos.fr/digitalguide/sites-internet/developpement-web/paradigmes-de-programmation/)
[^3]: Source : [Wikipedia : Programmation impérative](https://fr.wikipedia.org/wiki/Programmation_imp%C3%A9rative)
