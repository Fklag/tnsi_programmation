hide: - navigation  in docs.md
# Documents officiels

## Programme officiel

{{ telecharger("Le programme de spécialité de numérique et sciences informatiques de terminale générale","./officiels/ProgNSITerminale.pdf")}}

## Sujets de Baccalauréat (épreuve écrite)

### Année 2021

#### Amérique du nord

{{ telecharger("Amérique du nord 2021","./officiels/Annales/EE/2021/AmeriqueNord2021.pdf")}}

* **Exercice 1**  : bases de données relationnelles et langage {{ sc("sql") }}.
* **Exercice 2**  : routage, processsus et systèmes sur puces.
* **Exercice 3**  : tableaux et programmation de base en Python.
* **Exercice 4**  : arbres binaires et algorithmes associés.
* **Exercice 5**  : notion de pile, de file et programmation de base en Python.

#### Métropole sujet 1

{{ telecharger("Métropole 2021 - Sujet 1","./officiels/Annales/EE/2021/Metropole2021-1.pdf")}}
 
* **Exercice 1**  : bases de données.
* **Exercice 2**  : notions de piles et programmation orienté objet.
* **Exercice 3**  : gestion des processus et protocoles de routage.
* **Exercice 4**  : algorithme et programmation en Python.
* **Exercice 5**  : manipulation de tableaux, récursivité, méthode "diviser pour régner".

#### Métropole sujet 2

{{ telecharger("Métropole 2021 - Sujet 2","./officiels/Annales/EE/2021/Metropole2021-2.pdf")}}
 
* **Exercice 1**  : bases de données relationnelles et langage {{ sc("sql") }}.
* **Exercice 2**  : gestion des processus et des ressources par un système d'exploitation.
* **Exercice 3**  : arbres binaires de recherche et programmation orientée objet.
* **Exercice 4**  : programmation et récursivité.
* **Exercice 5**  : programmation.

#### Métropole sujet 2

{{ telecharger("Polynésie 2021","./officiels/Annales/EE/2021/Polynesie2021.pdf")}}
 
* **Exercice 1**  : Algorithmique et programmation (algorithmes de tri)
* **Exercice 2**  : Données en table, bases de données.
* **Exercice 3**  : arbres binaires de recherche et programmation orientée objet.
* **Exercice 4**  : Routage, architecture matérielle.
* **Exercice 5**  : Données en table, bases de données.

## Sujets de Baccalauréat (épreuve pratique)

!!! Danger "Attention"
    Des erreurs figurent dans certains des sujets.


{{ ep(2021) }}