# Révisions

## Les types simples et les types construits

Depuis la première nous savons comment sont codées les données au sein d'un ordinateur et nous avons utilisé des types simples et composés de Python.

=== "Les types simples"

    !!! info "Un objet de type simple ne stocke qu'une donnée"

        **entier** : `#!python (int) 1 12 -4`

        **nombre flottant** : `#!python (float): 1.1 12.0 -25E2`

        **chaîne de caractères** : `#!python (str): "Du texte" 'Un autre texte'`

        **booléen** : `#!python (bool) True False`

=== "Les types construits"

    !!! info "Les types construits permettent de stocker des collections de données"

        **tableau** `#!python (list): L = [1, 12, -4]`

        On accède aux éléments du tableau par index. `#!python L[2] → -4`

        **dictionnaire** `#!python (dict): D = {"nom": "Gaston", "age": 25}`

        On accède aux éléments du dictionnaire par clef. `#!python D["age"] → 25`

{{IDEv()}}

??? {{exercice()}}

    Déterminez les types des variables ci-dessous : 


    === "Question" 

        | Types | int | float | str | bool |
        |:---------|:-------|:-------|:-----|:------|
        | `#!python p = 8` | {{tit('')}} | {{tit('')}} | {{tit('')}} | {{tit('')}} |
        | `#!python nom = "Von Neumann"` | {{tit('')}} | {{tit('')}} | {{tit('')}} | {{tit('')}} |
        | `#!python e = 2.7172` | {{tit('')}} | {{tit('')}} | {{tit('')}} | {{tit('')}} |
        | `#!python p = 8.0` | {{tit('')}} | {{tit('')}} | {{tit('')}} | {{tit('')}} |
        | `#!python huit = "8"` | {{tit('')}} | {{tit('')}} | {{tit('')}} | {{tit('')}} |
        | `#!python r = 0` | {{tit('')}} | {{tit('')}} | {{tit('')}} | {{tit('')}} |
        | `#!python r = -120000` | {{tit('')}} | {{tit('')}} | {{tit('')}} | {{tit('')}} |
        | `#!python arrivé = True` | {{tit('')}} | {{tit('')}} | {{tit('')}} | {{tit('')}} |
        | `#!python huit = "8.0"` | {{tit('')}} | {{tit('')}} | {{tit('')}} | {{tit('')}} |
        | `#!python mort = "False"` | {{tit('')}} | {{tit('')}} | {{tit('')}} | {{tit('')}} |

    === "Solution" 

        | Types | int | float | str | bool | explication |
        |:---------|:-------|:-------|:-----|:------|:------|
        | `#!python p = 8` | {{tit('x')}} | {{tit('')}} | {{tit('')}} | {{tit('')}} ||
        | `#!python nom = "Von Neumann"` | {{tit('')}} | {{tit('')}} | {{tit('x')}} | {{tit('')}} ||
        | `#!python e = 2.7172` | {{tit('')}} | {{tit('x')}} | {{tit('')}} | {{tit('')}} |Le séparateur décimal est un point.|
        | `#!python p = 8.0` | {{tit('')}} | {{tit('x')}} | {{tit('')}} | {{tit('')}} |Le séparateur décimal est un point.|
        | `#!python huit = "8"` | {{tit('')}} | {{tit('')}} | {{tit('x')}} | {{tit('')}} |Les guillemets indiquent un `#!python str`.|
        | `#!python r = 0` | {{tit('x')}} | {{tit('')}} | {{tit('')}} | {{tit('')}} |`#!python 0` est un entier.|
        | `#!python r = -120000` | {{tit('x')}} | {{tit('')}} | {{tit('')}} | {{tit('')}} |`r` est un entier négatif donc relatif.|
        | `#!python arrivé = True` | {{tit('')}} | {{tit('')}} | {{tit('')}} | {{tit('x')}} |`#!python True` et `#!python False` sont des mots réservés.|
        | `#!python huit = "8.0"` | {{tit('')}} | {{tit('')}} | {{tit('x')}} | {{tit('')}} |Les guillemets indiquent un `#!python str`.|
        | `#!python mort = "False"` | {{tit('')}} | {{tit('')}} | {{tit('x')}} | {{tit('')}} ||

    ??? {{ext()}} 
        Pour connaitre le type référencée par une variable, on utilise l'instruction `#!python type(......)`. Par exemple, `#!python type(6100)` renvoie `#!python <class 'int'>`.

## Méthode d'itérations

!!! question "Qu'est-ce qu'un itérable ?"
        Un **itérable** est une représentation simple d'une série d'éléments que l'on peut parcourir. 

        Il s'agit par exemple d'une **chaîne de caractères** ou d'un **tableau**, car ceux-ci sont composés d'éléments que l'on peut **épeler** : la lettre ou l'élément.

        Les types `#!python str`, `#!python tuple`, `#!python list`, `#!python dict` et `#!python set` sont des itérables bien connus.

??? {{ext()}}

    Dans le détail, un itérable peut être créé en Python avec la fonction `#!python iter()`. 
        
    On itère sur l'itérable à l'aide de la fonction `#!python next()`. 

    - [ ] Lancer le code ci-dessous et dans la console de l'éditeur ci-contre, écrire `#!python next(iterable)`.
    - [ ] Répéter cette commande et observer le mot en train d'être épelé. La valeur référencée par la variable `#!python lettre` de la boucle `#!python for` est donc définie grâce à un appel à `#!python next()` d'un itérateur.
    - [ ] Remarquer l'erreur `#!python StopIteration` qui apparaît une fois l'itérable complètement épelé.
    - [ ] Modifier `#!python mot ='python'` en `#!python mot = 12345` et réitérer l'itération. Cela fonctionne-t-il ? 

    {{IDEv('python3/exemple4')}}


!!! warning "Itération sur une chaîne ou sur un tableau"
    On peut itérer sur les valeurs ou sur les index.

=== "Itération sur les valeurs"

    !!! info "On fait une itération sur les valeurs en utilisant le mot-clé `#!python in`"      

        ```py3
        for val in ma_liste:
            print(val)
        ```
=== "Itération sur les index"

    !!! info "On a également accès à chaque valeur en utilisant son index"
        C'est la méthode classique utilisée dans les langages impératifs.
        ```py3
        for i in range(len(ma_liste)):
            val = ma_liste[i]
            print("indice:", i, "valeur:", val)
        ```

=== "Exemple 1 (important!)"
    ???+ example "Exemple 1 (important!)"
        - [ ] Tester le code ci-dessous.
        - [ ] Que fait le code ? Est-ce conforme à notre définition d'un itérable ?

        {{IDEv('python3/exemple2')}}

        Pour vous aider à mieux comprendre : [PythonTutor](https://pythontutor.com/visualize.html#code=print%28%22It%C3%A9ration%20sur%20les%20caract%C3%A8res%20d'une%20cha%C3%AEne%22%29%0A%0Afor%20lettre%20in%20'PYTHON'%3A%0A%20%20%20%20print%28lettre%29%0A%0Aprint%28'Fin%20de%20la%20boucle%20inconditionnelle'%29&cumulative=true&curInstr=0&heapPrimitives=true&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false){target="_blank"}

        {{IDEv('python3/exemple02')}}

        Pour vous aider à mieux comprendre : [PythonTutor](https://pythontutor.com/visualize.html#code=print%28%22It%C3%A9ration%20sur%20les%20index%20d'une%20cha%C3%AEne%22%29%0A%0Ach%20%3D%20'PYTHON'%0Afor%20i%20in%20range%28len%28ch%29%29%3A%0A%20%20%20%20print%28ch%5Bi%5D%29%0A%0Aprint%28'Fin%20de%20la%20boucle%20inconditionnelle'%29&cumulative=true&curInstr=0&heapPrimitives=true&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false){target="_blank"}

=== "Exemple 2 (important!)"
    ???+ example "Exemple 2 (important!)"
        - [ ] Tester le code ci-dessous.
        - [ ] Que fait le code ? Est-ce conforme à notre définition d'un itérable ?

        {{IDEv('python3/exemple3')}}
        
        Pour vous aider à mieux comprendre : [PythonTutor](https://pythontutor.com/visualize.html#code=print%28%22It%C3%A9ration%20sur%20les%20%C3%A9l%C3%A9ments%20d'un%20tableau%20d'entiers%22%29%0A%0Afor%20element%20in%20%5B4,%203,%208,%2012,%2031%5D%3A%0A%20%20%20%20print%28element%29%0A%0Aprint%28'Fin%20de%20la%20boucle%20inconditionnelle'%29&cumulative=true&curInstr=0&heapPrimitives=true&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false){target="_blank"}

        {{IDEv('python3/exemple03')}}

        Pour vous aider à mieux comprendre : [PythonTutor](https://pythontutor.com/visualize.html#code=print%28%22It%C3%A9ration%20sur%20les%20index%20d'un%20tableau%20d'entiers%22%29%0A%0AL%20%3D%20%5B4,%203,%208,%2012,%2031%5D%0Afor%20i%20in%20range%28len%28L%29%29%3A%0A%20%20%20%20print%28L%5Bi%5D%29%0A%0Aprint%28'Fin%20de%20la%20boucle%20inconditionnelle'%29&cumulative=true&curInstr=0&heapPrimitives=true&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false){target="_blank"}
  



!!! warning "Itération sur les dictionnaires"
    Les dictionnaires étant des associations de clés(`#!python keys`) et de valeurs(`#!python values`),  
    on peut itérer sur les clés, les valeurs, ou les deux.

Dans la suite `#!python D` est un dictionnaire.

=== "Itération sur les clés : `#!python keys()`"

    !!! info "On fait une itération sur les clés en utilisant la méthode `#!python keys()`"      

        ```py3
        for cle in D.keys():
            print(cle)

        # Ou plus simplement.
        for cle in D:
            print(cle)
        ```
=== "Itération sur les valeurs: `#!python values()`"

    !!! info "On fait une itération sur les valeurs en utilisant la méthode `#!python values()`"

        ```py3
        for val in D.values():
            print(val)
        ```
=== "Itération sur les paires clé, valeurs: `#!python items()`"

    !!! info "On fait une itération sur les paires 'clé,valeur' en utilisant la méthode `#!python items()`"

        ```py3
        for cle, val in D.items():
            print(cle, '=>', val)
        ```
              