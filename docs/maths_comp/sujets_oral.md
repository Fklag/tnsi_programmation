---
title:  Sujets Oral Mathématiques Complémentaires
layout: parc
---



!!! info "Sujet 1"

    Pourquoi le format A4 ?


    __Ressource :__

    ??? video

        <iframe width="1008" height="567" src="https://www.youtube.com/embed/sgsJuqaiCCM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



!!! info "Sujet 2"

    La place des femmes en sciences à travers l'exemple de Sophie Germain mathématicienne au début du XIX ème siècle.


    __Ressources :__  
    
    * un podcast  <https://podcast.ausha.co/sophie-germain-project>
    * article Wikipedia <https://fr.wikipedia.org/wiki/Sophie_Germain>



!!! info "Sujet 3"

    Isaac Newton  et l'invention du calcul différentiel  

    __Ressources :__  
    
    * podcast : <https://hist-math.fr/newtona-auto#/>
    * pdf : <https://hist-math.fr/users/Histoires/Analyse/newtona_double.pdf>



!!! info "Sujet 4"

    Les modèles d'évolution de population de Malthus et Verhulst.
    
    __Ressources :__

    *  document d'accompagnement d'enseignement scientifique <https://www.pedagogie.ac-nantes.fr/medias/fichier/ra20-lycee-g-t-es-sous-theme-3-4-modeles-demographiques-1238222_1581925547124-pdf?ID_FICHE=539929&INLINE=FALSE>


!!! info "Sujet 5"

    Suites géométrique et croissance exponentielle, "la légende de Sissa".
    
    
    __Ressources :__

    * un podcast : <https://hist-math.fr/sissa-auto#/>
    * un pdf : <https://hist-math.fr/users/Histoires/Arithmetique/sissa_double.pdf>


!!! info "Sujet 6"

    Moyennes arithmétique et géométrique.
    
    
    __Ressources :__

    * un article sur le site [Images des mathématiques](https://images.math.cnrs.fr/Moyennes-III-la-moyenne-geometrique.html)
    * Sujet de l'épreuve 2 du Capes Externe 2020, Problème 1, partie A, problèmes 1 et 2  : <https://www.apmep.fr/IMG/pdf/epreuve_2_DV_CAPES_7_juillet_2020.pdf>




!!! info "Sujet 7"

    Archimède et la quadrature de la parabole.    
    
    __Ressources :__

    * Podcast avec preuve simplifiée d'Archimède : <https://hist-math.fr/zenon-auto#/19>
    * Preuve d'Archimède (avec double démonstration par l'absurde) : <http://mathematiques.ac-bordeaux.fr/lycee2010/mathsettice/terminale/analyse/qua_para/qua_parab.pdf>


    
    
    __Ressources :__

    * Un article sur le site [Culture Maths](https://culturemath.ens.fr/thematiques/mathematiques-appliquees/propagation-d-epidemies)




!!! info "Sujet 8"

    Le modèle SIR de propagation d'une épidémie
    
    
    __Ressources :__

    * Un article sur le site [Culture Maths](https://culturemath.ens.fr/thematiques/mathematiques-appliquees/propagation-d-epidemies)



!!! info "Sujet 9"

    Calculer le coût d'un crédit immobilier.

    __Ressources :__

    * Un article sur le site [Culture Maths](https://culturemath.ens.fr/thematiques/concours-d-enseignement/les-cles-du-credit)



!!! info "Sujet 10"

    Probabilités et système électoral : le paradoxe de Condorcet.

    __Ressources :__

    * Sur le paradoxe de Condorcet : <https://scienceetonnante.com/2012/04/23/le-paradoxe-de-condorcet/>
    * Sur le théorème d'Arrow : <http://images.math.cnrs.fr/A-vote.html>
    