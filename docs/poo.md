
{% set num = 5 %}
{% set titre = "Notions de programmation orienté objet" %}
{% set theme = "python" %}

{{ titre_chapitre(num,titre,theme)}}

??? example "Références au BO" 
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;border-style:1 px solid}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-cv16{font-weight:bold;background-color:#dae8fc;border-color:inherit;text-align:center}
    .tg .tg-xldj{border-color:inherit;text-align:left}
    </style>

    === "Structures de données"
        
        <table class="tg">
        <tr>
            <th class="tg-cv16">Contenus</th>
            <th class="tg-cv16">Capacités attendues</th>
            <th class="tg-cv16">Commentaires</th>
        </tr>
        <tr>
            <td class="tg-xldj">Vocabulaire de la programmation objet : classes, attributs, méthodes, objets.</td>
            <td class="tg-xldj">Écrire la définition d'une classe.<br>Accéder aux attributs et méthodes d'une classe.</td>
            <td class="tg-xldj">On n'aborde pas ici tous les aspects de la programmation objet comme le polymorphisme et l'héritage.</td>
        </tr>
        </table>

    === "Langages et programmation"

        <table class="tg">
        <tr>
            <th class="tg-cv16">Contenus</th>
            <th class="tg-cv16">Capacités attendues</th>
            <th class="tg-cv16">Commentaires</th>
        </tr>
        <tr>
            <td class="tg-xldj">Paradigmes de programmation.</td>
            <td class="tg-xldj">Distinguer sur des exemples les paradigmes impératif, fonctionnel et objet.<br>Choisir le paradigme de programmation selon le champ d'application d'un programme.</td>
            <td class="tg-xldj">Avec un même langage de programmation, on peut utiliser des paradigmes différents. Dans un même programme, on peut utiliser des paradigmes différents.</td>
        </tr>
        </table>

??? example "Un premier exemple issu de Scratch"
    Observons ce jeu assez minable en Scratch (non, mais vraiment, allez voir sur le site...).
    <p align="center">
    <iframe src="https://scratch.mit.edu/projects/568504906/embed" allowtransparency="true" width="485" height="402" frameborder="0" scrolling="no" allowfullscreen></iframe>
    </p>

    Le lien [ici](https://scratch.mit.edu/projects/568504906){ : target="_blank" }

    <table class="tg">
        <tr>
            <th class="tg-cv16">Ce jeu sommaire est construit autour de trois éléments :</th>
            <th class="tg-cv16">Chacun de ces trois éléments possède :</th>
        </tr>
        <tr>
            <td class="tg-xldj"> 
              <ul>
              <li> la chauve-souris ; </li> 
              <li>  l'éclair ; </li> 
              <li>  le chat. </li> </ul></td>
            <td class="tg-xldj">
              <ul>
              <li> sa propre zone de script; </li> 
              <li> ses propres caractéristiques (nom, taille, costumes, position de départ, orientation...). </li> </ul></td>
        </tr>
        </table>

    Et ces trois éléments réagissent en fonction **d'événements** liés soit à l'action du joueur, soit à leurs propres interactions.

    L'éclair et le chat ont la possibilité d'exister sous la forme de ==**clones**==, chacun des clones ayant ==**ses propres caractéristiques**== bien que **partageant le même comportement**.

    Aussi simpliste que Scratch paraisse, il est néanmoins un **langage de programmation** ==**multiparadigme**== : il est en effet conçu pour gérer la programmation **impérative**, la programmation **orientée objet** ainsi que la programmation **événementielle**.

    ??? aide "Paradigmes de programmation"

        Un paradigme est *"une représentation du monde, une manière de voir les choses, un modèle cohérent du monde qui repose sur un fondement défini"*.[Wkipedia](https://fr.wikipedia.org/wiki/Paradigme#Paradigme_en_linguistique){ : target="_blank" }.
        
        En programmation, plus précisément, on parle de **paradigmes de programmation** pour exprimer la manière dont sont conçus et envisagés les programmes.
        On distingue entre autres :
        
        * le paradigme de la **programmation impérative**, qui est celui que nous avons utilisé jusqu'ici : on décrit les opérations en séquences d'instructions exécutées par l'ordinateur dans un ordre précis (que le langage soit compilé ou interprété). C'est le paradigme classique, celui auquel tout le monde pense quand on parle de programme informatique.
        * le paradigme de la **programmation orienté objet**, qui "consiste en la définition et l'interaction de briques logicielles appelées objets ; un objet représente un concept, une idée ou toute entité du monde physique, comme une voiture, une personne ou encore une page d'un livre. Il possède une structure interne et un comportement, et il sait interagir avec ses pairs. Il s'agit donc de représenter ces objets et leurs relation[...]". [Wikipedia](https://fr.wikipedia.org/wiki/Programmation_orient%C3%A9e_objet).	
        * le paradigme de la **programmation fonctionnelle**, qui considère le calcul en tant qu'évaluation de fonctions mathématiques.
        * le paradigme de la **programmation événementielle**, qui est fondé sur la notion d'événements. Le programme sera principalement défini par ses réactions aux différents événements qui peuvent se produire, c'est-à-dire des changements d'état de variable, par exemple l'incrémentation d'une liste, un déplacement ou un click de souris, une saisie au clavier... 
        * et [bien d'autres](https://fr.wikipedia.org/wiki/Paradigme_(programmation)){ : target="_blank" }...
        
    ??? attention "Remarque"
        Les différents langages de programmation peuvent être plus ou moins spécialisé selon un certain nombre de paradigmes. Quasiment tous respectent le **paradigme originel impératif**.  
        Certains langages ne dépassent pas ce paradigme (assembleur, Fortran, Algol, BASIC,... ).  
        D'autres sont spécialisés sur un paradigme spécifique ( comme Lisp, ML, OCaml en programmation fonctionnelle, Ada, Smalltalk, C++, Ruby, C# Swift... en POO).  
        Mais en général tous les langages cités possèdent des composantes **multiparadigmes**.  
        C'est le cas de `Python`, qui supporte la programmation :

        * impérative ;
        * fonctionnelle ;
        * procédurale ;
        * orientée objet ;
        * et en partie événementielle.


## Un nouveau paradigme de programmation :


Jusqu'ici, les programmes que vous avez écrits ont été réalisés en **programmation impérative** : ils étaient constitués d'instructions qui se succédaient les unes après les autres, dans l'ordre où vous les aviez placées dans le code.

Une variante de la programmation impérative, et que vous avez aussi utilisée, est la **programmation procédurale** : chaque programme a été décomposé en plusieurs fonctions réalisant des tâches simples.
!!! aide "programmation procédurale"
    En programmation procédurale, ce sont les **fonctions** qui sont au coeur du programme ; elles s'appliquent à modifier des données.

Lorsque plusieurs programmeurs travaillent simultanément sur un gros projet, pour éviter les conflits entre les fonctions codées par chacuns, à partir des années 60, on a imaginé une **programmation orientée objet**, la **POO**.
!!! aide "programmation orientée objet"
    En programmation orientée objet, ce sont les **données** qui sont au coeur du programme : celles-ci vont désormais être protégées et c'est le développeur qui décide comment elles seront créees, accessibles, modifiées, ...

Le premier langage de programmation initiant une forme de programmation orientée objet fut Simula, créé à partir de 1962. Ce langage servait à faciliter la programmation de logiciels de simulation.
Le premier langage de programmation réellement fondé sur la programmation orientée objet fut Smalltalk 71, créé au début des années 70.

De très nombreux langages actuels fournissent les concepts de la POO, mais n'obligent cependant pas à l'utiliser; c'est le cas de `Python` par exemple.

La POO n'est pas un nouveau "langage" de programmation à apprendre; c'est une autre façon d'aborder la conception d'un programme et de le coder, on parle d'un paradigme de programmation.

!!! bug "Conclusion"
    Les failles de la programmation procédurale posent le besoin de la programmation orientée objet. La programmation orientée objet corrige les défauts du programmation procédurale en introduisant le concept «**objet**» et «**classe**». Il améliore la sécurité des données, ainsi que l'initialisation et le nettoyage automatiques des objets. La programmation orientée objet permet de créer plusieurs instances de l'objet sans aucune interférence.

En appliquant la POO, vous allez pouvoir désormais définir vos propres objets informatiques en précisant leurs types, leurs propriétés et les fonctionnalités agissant dessus....

![poo](images/pooc.png){: .center .imagepng}
 
## Activités 

=== "Déjà vu"
    {{ titre_activite("Déjà vu",[],-1) }}

    Comme M.Jourdain faisait de la prose sans le savoir, vous avez déjà utilisé l'an passé la programmation objet.

    ??? "Principe"      
        Le concept principal de la **programmation orientée objet** (POO) est celui d' **==objet==**.
        Nous avons déjà rencontré un certain nombres d'objets prédéfinis dans Python :

        * la classe `int`, pour représenter les entiers relatifs ;
        * la classe `float`, pour représenter les nombre à virgule flottante (une partie des décimaux) ;
        * la classe `str` pour représenter les chaînes de caractères ;
        * la classe `list`, pour représenter un ensemble *ordonné* et *mutable* de toute autre collection d'objets ;
        * la classe `tuple`, pour représenter un ensemble *ordonné* et *non-mutable* de toute autre collection d'objets ;
        * la classe `set`, pour représenter un ensemble *non ordonné* et *sans doublons* ;
        * la classe `dict`, pour représenter une collection *non ordonnée* indexée par des clés.


        Chacun de ces types précédents possède ses propres **attributs** et ses propres **méthodes** :

        * un objet de la classe `str` possède une longueur, donné par la fonction built-in `len()`,
        et plusieurs **méthodes** associées comme `lower()`, `upper()`, etc... 
        *  un objet de la classe `list` possède lui aussi une longueur, et ses propres **méthodes**
        comme  `append()`, `pop()`, etc...
        
        ??? info "DUNDERS !!!"
            En fait la valeur renvoyée par la fonction *built-in* `len()` pour les types `str` et `list` correspond à celle calculée par le **DUNDERS** `__len__(self)`. Il s'agit donc du résultat renvoyé par une **méthode**. 

    Regardons par exemple comment est programmée la fonction `randint()` de la bibliothèque `random` :

    {{IDEv('poo/dejavu')}}

    Excepté la présence d'un `self` surprenant (`#!python randint` n'ayant besoin que de 2 paramètres `a` et `b`), rien de nouveau, on voit la définition d'une fonction très courte...

    > Inspectons maintenant tout le code de la bibliothèque random, on constate la présence de mots-clés comme `class`, une fonction `init` et si vous chercher bien on y retrouve la fonction `#!python randint`... mais inclue dans la définition de la classe... 

    {{IDE('poo/inspe')}}

    Cette façon d'organiser du code appartient à la POO !

    > En Python, tout est objet !! Par exemple le module  `math` est un objet, et possède donc :

    > - des attributs : `pi`, ...  
      - des méthodes : `cos()`, ...  
       
    > Alors en [Python tout serait objet ?](https://python.developpez.com/cours/DiveIntoPython/php/frdiveintopython/getting_to_know_python/everything_is_an_object.php#:~:text=Mais%20tout%20est%20objet%20dans,Les%20cha%C3%AEnes%20sont%20des%20objets.)
    > Voir le [notebook Intro_POO.ipynb](notebook/Intro_POO.ipynb){: target="_blank" } pour plus de détails ou télécharger le au format `.zip` avec le lien ci-dessous : {{ televerser("Jupyter Notebook","https://lgt-beaussier-83512-moodle.atrium-sud.fr/mod/resource/view.php?id=15935")}}.

=== "Introduction à la programmation orientée objet"
    {{ titre_activite("Introduction à la programmation orientée objet",[],0) }}
    {{ televerser("Jupyter Notebook","https://lgt-beaussier-83512-moodle.atrium-sud.fr/mod/resource/view.php?id=15581")}}

=== "Syntaxe objet en Python"
    {{ titre_activite("Syntaxe objet en Python",[]) }}

    !!! example "Papier/Crayon"

        1. On donne le code python suivant :

            ```py3
            class Duree:

            def __init__(self,heures,minutes,secondes):
                self.heures=heures
                self.minutes=minutes
                self.secondes=secondes

            def __str__(self):
                return f"{self.heures}h {self.minutes}m {self.secondes}s"
            ```
            a. Quelle classe est créée ? Quels sont les attributs des objets de cette classe ?  
            b. Quelle méthode est crée pour les objets de cette classe ?  

        2. Le record du monde du marathon est actuellement de deux heures, une minute et trente neuf secondes, créer à l'aide de la classe ci-dessus un objet `record_marathon` représentant cette durée et l'afficher.  
        {{IDE('poo/duree')}}

    ??? "Les ingrédients de la POO"
        La POO est un paradigme de programmation qui consiste à mettre en relation des objets qui représente (en général) des concepts réels (par exemple: une voiture, un livre, une personne).

        !!! aide
            Un **objet** est une structure de donnée qui regroupe en son sein deux types d'entités:

            - des données, appelées des **attributs**;  
            - des fonctions constituant l'interface de cet objet, appelées des **méthodes**.

        C'est ce regroupement qui a constitué une véritable révolution dans les années 60. Il faut le mettre en comparaison avec les paradigmes alors existants, à savoir la programmation impérative et fonctionnelle.  
        Les avantages sont multiples:  
        
        - On regroupe dans une seule entité à la fois les données et les fonctions permettant d'interagir avec ces données;  
        - Ce regroupement permet notamment de cacher les détails de l'implémentation de l'objet à l'utilisateur, ce qui offre une grande souplesse au programmeur (notamment celle de complètement changer ces détails d'implémentations sans affecter l'interface de l'objet). Ce concept très important s'appelle l'**encapsulation**.

        !!! aide "Encapsulation"
            En POO, le principe est que les attributs d'un objet lui sont privés, c'est à dire "visibles" uniquement par lui (et donc inaccessibles "de l'extérieur"), c'est à dire le programmeur qui a instancié un objet de cette classe. ( = non publics ) : c'est le principe d'encapsulation des données.

        - La POO comporte d'autres aspects (polymorphisme, héritage), mais ils ne seront pas étudiés en terminale NSI. Ils ont clairement leur utilité, mais le langage `Python` ne fait que modérément appel à eux dans la pratique (l'aspect très dynamique du langage `Python` offre d'autres avantages).

    ??? "Le concept de classe"
        La plupart des langages à objets utilisent le concept de classe. 
        
        !!! aide
            Une **classe** est une définition abstraite d'un objet, une sorte de plan qui va servir ensuite à construire des représentations concrètes des objets.

        D'autres langages utilisent plutôt le concept de [prototype](https://fr.wikipedia.org/wiki/Programmation_orient%C3%A9e_prototype){target = _blank}, comme par exemple le très célèbre [javascript](https://fr.wikipedia.org/wiki/JavaScript){target = _blank}: pour ces derniers, les objets sont directement construits à partir d'autres objets, dans une sorte de mécanisme de clônage (mais qui peut aussi comporter des mutations pour obtenir de nouvelles fonctionnalités).

        !!! bug "En `Python`, la classe décrit précisément la structure des objets qu'elle va représenter"
            - Une méthode particulière `#!python __init__`, appelée le **constructeur**, permet notamment d'initialiser l'objet et ses différents attributs;
            - Les **méthodes** qui serviront d'interface pour cet objet sont aussi définies au sein de la classe.
            - On peut aussi définir des méthodes qui n'auront qu'un usage *interne* à l'objet. Elle ne font techniquement pas partie de l'*interface* de celui-ci, mais seront très utiles pour son *implémentation*.

        ??? example "Une classe `Rectangle`"
            On souhaite définir une classe qui représentera un rectangle (au sens mathématique). Plusieurs représentations équivalentes sont possibles, nous choisissons de caractériser un rectangle par ses coordonnées en haut à gauche et en bas à droite.
            
            ![rectangle](assets/images/rect.png){: .center .width=50% }
            
            Notre objet comportera donc naturellement 4 attributs correspondant aux abscisses et aux ordonnées de ces deux points.

            ```py3
            class Rectangle:
                def __init__(self, x1, y1, x2, y2):
                    self.x1 = x1
                    self.y1 = y1
                    self.x2 = x2
                    self.y2 = y2
            ```

            Nous venons de définir une classe minimaliste: elle ne comporte qu'une unique méthode, le constructeur `#!python __init__(self, x1, x2, y1, y2)`.  
            Le premier paramètre d'une méthode (conventionnelement toujours appelé `self`, mais ce n'est pas une obligation) désigne toujours l'objet qui sera défini grâce à la classe.  
            La syntaxe `self.x1 = x1` permet ici de créer un nouvel attribut `x1` pour l'objet `self`, qui aura sa valeur initiale donnée par le paramètre `x1` du même nom passé au constructeur.

            Voyons comment on peut créer un objet:

            ```py3
            >>> r = Rectangle(3, 4, 12, 9)
            >>> r.x1
            3
            >>> r.x2
            12
            >>> r.x1 = 5
            >>> r.x1
            5
            ```

            On constate que l'on peut créer un objet en appelant sa classe, et en passant tous les paramètres **sauf le paramètre `self`** qui sera toujours passé automatiquement.

            Examinez plus précisément ce mécanisme à l'aide de PythonTutor. 
            
            <iframe width="1200" height="450" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=class%20Rectangle%3A%0A%20%20%20%20def%20__init__%28self,%20x1,%20y1,%20x2,%20y2%29%3A%0A%20%20%20%20%20%20%20%20self.x1%20%3D%20x1%0A%20%20%20%20%20%20%20%20self.y1%20%3D%20y1%0A%20%20%20%20%20%20%20%20self.x2%20%3D%20x2%0A%20%20%20%20%20%20%20%20self.y2%20%3D%20y2%0A%0Ar%20%3D%20Rectangle%283,%204,%2012,%209%29%0Aprint%28r.x1%29%0Aprint%28r.x2%29%0Ar.x1%20%3D%205%0Aprint%28r.x1%29&cumulative=false&heapPrimitives=false&mode=edit&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

            > Rien ne vaut une visualisation dynamique de l'exécution, mais voici quelques explications:

            - Lors de la création de l'objet, une nouvelle **instance** (c'est-à-dire une réalisation physique de la classe) est crée en mémoire, et est référencée par la variable `self` dans l'environnement d'exécution du constructeur `#!python __init__`.  
              Remarquez que les coordonnées du rectangle sont bien passées en paramètre, mais les attributs ne sont pas encore créés.    
            - À la fin du constructeur, les quatre attributs ont été créés. Ils portent le même nom que les paramètres du constructeur (ce n'était d'ailleurs pas une obligation, mais cela ne pose aucune difficulté), mais le fait qu'ils soient préfixés par `self.` enlève toute ambiguïté.  
            - Une fois le rectangle créé, les attributs peuvent être manipulés grâce à la syntaxe utilisée depuis bien longtemps en `Python`.  
              À présent on comprend pourquoi dans certains cas des fonctions étaient précédées d'une valeur, comme par exemple pour `#!python ma_liste.append(42)`: il s'agissait en fait de méthodes attachées à un objet particulier (en l'occurence ici un objet de classe `list`).

            La classe que nous venons de créer est extrêmement minimaliste: on se contente de créer des attributs dans le constructeur, et c'est tout.  
            Notons que nous disposons néanmoins d'une nouvelle fonctionnalité intéressante: un objet rectangle, avec quatre attributs portant un nom (que l'on espère) intelligible et identifiable. C'est déjà bien plus lisible que la syntaxe `rectangle[0]`, `rectangle[1]`, `rectangle[2]`, `rectangle[3]` que nous aurions dû utiliser si nous avions opté pour une liste ou bien un tuple.

            Voyons à présent comment étoffer l'interface de notre objet. Pour cela, nous allons devoir lui ajouter de nouvelles méthodes !

        ??? example "Ajout de méthodes à la classe Rectangle"
            Quelle genre d'interface pourrions-nous souhaiter pour un rectangle ?  
            On peut notamment vouloir connaître sa longueur, sa hauteur, son périmètre, son aire. Rien de plus simple !

            ```py3
            class Rectangle:
            ...

            def longueur(self):
                return self.x2 - self.x1

            def hauteur(self):
                return self.y2 - self.y1

            def perimètre(self):
                return 2*(self.hauteur() + self.longueur())

            def aire(self):
                return self.hauteur()*self.longueur()
            ```

            Nous n'avons pas reproduit le constructeur ici, puisqu'il n'a pas été modifié.

            Nous disposons à présent de quatre nouvelles méthodes, qui permettent d'accéder à certaines fonctionnalités de notre rectangle.
            
            ```py3
            >>> r = Rectangle(3, 4, 12, 9)
            >>> r.longueur()
            9
            >>> r.hauteur()
            5
            >>> r.périmètre()
            28
            >>> r.aire()
            45
            ```

            Vous pouvez observer comment ces valeurs sont calculées avec PythonTutor.

            <iframe width="1200" height="650" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=class%20Rectangle%3A%0A%20%20%20%20def%20__init__%28self,%20x1,%20y1,%20x2,%20y2%29%3A%0A%20%20%20%20%20%20%20%20self.x1%20%3D%20x1%0A%20%20%20%20%20%20%20%20self.y1%20%3D%20y1%0A%20%20%20%20%20%20%20%20self.x2%20%3D%20x2%0A%20%20%20%20%20%20%20%20self.y2%20%3D%20y2%0A%20%20%20%20%20%20%20%20%0A%20%20%20%20def%20longueur%28self%29%3A%0A%20%20%20%20%20%20%20%20return%20self.x2%20-%20self.x1%0A%0A%20%20%20%20def%20hauteur%28self%29%3A%0A%20%20%20%20%20%20%20%20return%20self.y2%20-%20self.y1%0A%0A%20%20%20%20def%20perimetre%28self%29%3A%0A%20%20%20%20%20%20%20%20return%202*%28self.hauteur%28%29%20%2B%20self.longueur%28%29%29%0A%0A%20%20%20%20def%20aire%28self%29%3A%0A%20%20%20%20%20%20%20%20return%20self.hauteur%28%29*self.longueur%28%29%20%20%20%20%0A%0Ar%20%3D%20Rectangle%283,%204,%2012,%209%29%0Aprint%28r.longueur%28%29%29%0Aprint%28r.hauteur%28%29%29%0Aprint%28r.perimetre%28%29%29%0Aprint%28r.aire%28%29%29&cumulative=false&heapPrimitives=false&mode=edit&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

            Les quatre méthodes implémentées jusqu'à présent se contentaient d'accéder aux attributs pour en déduire de nouvelles caractéristiques du rectangle.

            Notez la présence du paramètre `self`, toujours situé en première position, et qui est toujours rajouté automatiquement par `Python` lors d'un appel.  
            Concrètement, l'appel `#!python r.longueur()` est automatiquement traduit par `Python` en `#!python Rectangle.longueur(r)`, et on comprend ainsi que la valeur que pointera `self` ne sera autre que l'objet `r`, c'est-à-dire l'objet qui se trouve juste avant le point dans la syntaxe `#!python r.longueur()`.

            > Mais on peut tout aussi bien **modifier** les valeurs d'un ou plusieurs attributs. 

            Ajoutons une méthode `#!python translation(dx, dy)` qui prend pour paramètre les coordonnées d'un vecteur, et déplace le rectangle suivant la translation définie par ce vecteur:

            ```py3
            class Rectangle:
            ...

            def translation(self, dx, dy):
                self.x1 = self.x1 + dx
                self.x2 = self.x2 + dx
                self.y1 = self.y1 + dy
                self.y2 = self.y2 + dy
            ```

            On se contente ici d'ajouter les coordonnées du vecteur aux deux coordonnées définissant notre rectangle.

            ```py3
            >>> r.translation(-5, 3)
            >>> r.x1, r.y1
            (-2, 7)
            >>> r.x2, r.y2
            (7, 12)
            ```


        ??? example "Une meilleure encapsulation"
            Le rectangle ainsi défini à la section précédente est déjà bien fonctionnel, mais son utilisation par le programmeur comporte quelques dangers dont il faut avoir conscience:

            - Idéalement, on ne devrait accéder à l'objet que par l'intermédiaire de son interface. Mais ici, il est tout à fait possible de modifier les attributs en dehors des méthodes de l'objet.

            - Un problème induit par le précédent: 
              nous avons fait l'hypothèse (hasardeuse) que les contraintes $x1 \leqslant x2$ et $y1 \leqslant y2$ seraient toujours respectées.  
              C'est notamment sous ces hypothèse que nous avons implémenté `longueur()` et `hauteur()`. Or, n'importe qui peut changer les attributs pour leur donner des valeurs ne respectant pas ces contraintes (voire, et c'est encore pire, des valeurs qui ne seraient même pas des nombres).

            - L'utilisateur de la classe `Rectangle` est constamment conscient de la présence de ces quatre attributs. Cela peut paraître anodin, mais c'est en fait un lourd handicap: le concepteur de la classe ne pourra plus changer les attributs sans risquer de casser tous les programmes utilisant cette classe.  
            Autrement dit: **les attributs font partie de l'interface de l'objet**.

            Illustrons ces points par un exemple:

            ```py3
            >>> r = Rectangle(12, 4, 3, 9):
            >>> r.longueur()
            -9
            >>> r.hauteur()
            5
            >>> r.périmètre()
            -8
            >>> r.aire()
            -45
            ```

            Comment pouvons-nous corriger tous ou partie de ces défauts ? Une technique très prisée par les programmeurs utilisant le paradigme objet est de s'astreindre à **cacher les attributs**.  
            D'ailleurs, la plupart des langages objets imposent ce genre de restriction par défaut (c'est le cas de `C++`, `Java` et `C#` notamment). En `Python`, langage dynamique et très permissif par essence, il est possible de cacher des attributs en suivant certaines conventions.

            !!! aide "Il y a essentiellement deux techniques utilisables"

                - On peut ajouter un simple caractère *underscore* `_` devant le nom de l'attribut.  
                  Cela n'empêche pas qui que ce soit d'accéder à cet attribut, mais cette convention est universellement respectée par les programmeurs Python qui savent ainsi que cet attribut n'est pas destiné à être accédé à la main, et qu'il pourra être modifié voire disparaître à tout moment.

                - `Python` propose un mécanisme un peu plus sûr, qui consiste à ajouter deux caractères *underscore* `__` devant un nom d'attribut.  
                  Le langage bloquera alors toute tentative d'accès à cet attribut (à moins d'utiliser des moyens très alambiqués), ce qui revient à créer des attributs *cachés*, ou plutôt **privés** selon la terminologie classique en POO.


            === "Utilisation du simple underscore"
                ???+ example "`_attrib #attribut protégé`"
                    Voyons comment nous pourrions améliorer notre classe `Rectangle` par la première méthode:

                    ```py3
                    class Rectangle:
                        def __init__(self, x1, y1, x2, y2):
                            # On teste la validité des paramètres:

                            # Les quatre multiplications (ridicules) ci-dessous
                            # ont une double et réelle utilité:
                            # - elles déclenchent une erreur si les paramètres
                            #   ne sont pas numériques (très bien !)
                            # - elles assurent que les paramètres soient des
                            #   flottants (un type numérique adapté à notre classe)
                            x1 = 1.0*x1
                            y1 = 1.0*y2
                            x2 = 1.0*x2
                            y2 = 1.0*y2

                            if x1 > x2:
                                # Les abscisses sont dans le mauvais ordre:
                                # on les échange
                                x1, x2 = x2, x1

                            if y1 > y2:
                                # Les ordonnées sont dans le mauvais ordre:
                                # on les échange
                                y1, y2 = y2, y1

                            self._x1 = x1
                            self._y1 = y1
                            self._x2 = x2
                            self._y2 = y2

                        def x1(self):
                            return self._x1

                        def y1(self):
                            return self._y1

                        def x2(self):
                            return self._x2

                        def y2(self):
                            return self._y2

                        def longueur(self):
                            return self._x2 - self._x1

                        def hauteur(self):
                            return self._y2 - self._y1

                        def translation(self, dx, dy):
                            self._x1 += dx
                            self._x2 += dx
                            self._y1 += dy
                            self._y2 += dy

                        def perimètre(self):
                            return 2*(self.hauteur() + self.longueur())

                        def aire(self):
                            return self.hauteur()*self.longueur()
                    ```

                    Les contraintes sont vérifiées lors de la création de l'objet. Certaines pourraient déclencher des erreurs (valeurs non numériques), d'autres seront corrigées automatiquement (coordonnées mal ordonnées).

                    !!! aide "Quelques points importants"
                        - Nous avons rajouté quatre méthodes permettant d'accéder (en lecture mais pas en écriture) aux quatre attributs.  
                        Notons qu'il n'y a aucun moyen de savoir si la méthode `#!python Rectangle.x1()` couvre un attribut ou si elle est calculée par un autre moyen. *Nous reviendrons sur ce point un peu plus loin*.
                        
                        - La seule méthode qui permet de modifier les attributs cachés est `#!python translation(dx, dy)`, et elle ne peut pas casser les contraintes établies au moment de la construction: l'utilisation de cette méthode est donc sûre.

                    > Notons qu'il est toujours possible de ne pas s'en tenir à l'interface proposée, et de continuer à modifier les valeurs des attributs malgré les avertissements implicites du concepteur. Mais ce sera à nos risques et périls !

                    ```py3
                    >>> r = Rectangle(3, 4, 12, 9)
                    >>> r._x1
                    3
                    >>> r._x1 = 21
                    >>> r.aire()
                    -45 
                    ```

            === "Utilisation du double underscore"
                ???+ example "`__attrib # attribut privé`"
                    En apparence, la modification semble légère:

                    ```py3
                    class Rectangle:
                        def __init__(self, x1, y1, x2, y2):
                            # On teste la validité des paramètres:

                            # Les quatre multiplications (ridicules) ci-dessous
                            # ont une double et réelle utilité:
                            # - elles déclenchent une erreur si les paramètres
                            #   ne sont pas numériques (très bien !)
                            # - elles assurent que les paramètres soient des
                            #   flottants (un type numérique adapté à notre classe)
                            x1 = 1.0*x1
                            y1 = 1.0*y2
                            x2 = 1.0*x2
                            y2 = 1.0*y2

                            if x1 > x2:
                                # Les abscisses sont dans le mauvais ordre:
                                # on les échange
                                x1, x2 = x2, x1

                            if y1 > y2:
                                # Les ordonnées sont dans le mauvais ordre:
                                # on les échange
                                y1, y2 = y2, y1

                            self.__x1 = x1
                            self.__y1 = y1
                            self.__x2 = x2
                            self.__y2 = y2

                        def x1(self):
                            return self.__x1

                        def y1(self):
                            return self.__y1

                        def x2(self):
                            return self.__x2

                        def y2(self):
                            return self.__y2

                        def longueur(self):
                            return self.__x2 - self.__x1

                        def hauteur(self):
                            return self.__y2 - self.__y1

                        def translation(self, dx, dy):
                            self.__x1 += dx
                            self.__x2 += dx
                            self.__y1 += dy
                            self.__y2 += dy

                        def perimètre(self):
                            return 2*(self.hauteur() + self.longueur())

                        def aire(self):
                            return self.hauteur()*self.longueur()
                    ```
                    Les remarques formulées à la section précédente sont toujours d'actualité. Mais il y a une différence fondamentale: il n'est plus possible d'accéder (facilement) aux **attributs cachés**:

                    ```py3
                    >>> r = Rectangle(3, 4, 12, 9)
                    >>> r.__x1
                    Traceback (most recent call last):
                    File "<stdin>", line 1, in <module>
                    AttributeError: 'Rectangle' object has no attribute '__x1'
                    ```
                    Pourtant, la classe fonctionne parfaitement: les attributs sont accessibles de manière interne, par les méthodes définissant la classe, mais pas depuis l'extérieur de celle-ci.

                    ??? example "Remarque"
                        Nous ne détaillerons pas trop ici le mécanisme utilisé par `Python` pour réaliser cela, ni comment le contourner.  
                        En `Python`, lorsque le nom d'un attribut commence par `__`, celui-ci est automatiquement renommé ainsi : `_nomClasse__nomAttribut`.  
                        Étant ainsi renommé, il n'est plus aussi aisément accessible depuis l'extérieur de la classe. 

                        !!! aide "Accesseur"
                            Pour pouvoir obtenir (*accès en lecture*) la valeur de l'attribut, on créé dans la classe une méthode appelée **accesseur**.  
                            Par convention, un accesseur commence par le verbe anglais `get`. 

                            ```py3
                            def get_x1(self):   # self toujours pour désigner l'objet auquel s'appliquera cette méthode
                                return self.__x1 
                            ```

                        !!! aide "Mutateur"
                            Pour pouvoir modifier (*accès en écriture*) la valeur de l'attribut d'un objet, on créé dans la classe une méthode appelée **mutateur**.  
                            Par convention, un mutateur commence par le verbe anglais `set`.

                            ```py3
                            def set_x1(self,valeur):
                                self.__x1 = valeur 
                            ```

                    À noter que la plupart des programmeurs `Python` n'utilisent pas ce mécanisme, préférant le précédent: le fait de cacher des attributs, ou plutôt de les rendre privés, n'est pas dans un but de sécurité mais simplement de mise en garde contre une utilisation non souhaitée de l'objet. On part toujours du principe que les programmeurs feront preuve de bon sens.

                    En particulier, ces mécanismes (et ce, quel que soit le langage à objet) n'ont absolument pas pour objectif de protéger contre une tentative d'intrusion par un hacker, par exemple. Ils n'offrent absolument **aucune** protection dans ce domaine.





        ??? example "Changer l'implémentation d'un objet"
            Le fait de bien respecter les principes de l'encapsulation offre une grande souplesse au programmeur.  
            Notamment, il est possible de changer les détails de l'implémentation d'un objet **sans en changer l'interface**. Ce faisant, tous les utilisateurs de notre classe pourront continuer à l'utiliser sans le moindre changement.

            !!! aide "Pourquoi voudrait-on changer l'implémentation d'un objet ?"

                - Une nouvelle implémentation pourrait être beaucoup plus simple du point de vue du programmeur.
                
                - Une nouvelle implémentation pourrait utiliser un algorithme ou bien une structure de données beaucoup plus efficaces.  
                Passer d'un algorithme quadratique comme par exemple un tri par insertion ou par sélection à un algorithme plus efficace comme un tri par fusion peut changer l'efficacité d'une méthode de manière déterminante.  
                Il n'est pas toujours simple de trouver des algorithmes plus efficaces, et ils s'accompagnent en général d'une implémentation plus complexe, mais le gain en efficacité justifie quasiment systématiquement les efforts nécessaires (choix de l'algorithme, éventuellement nouvelles structures de données, implémentation, tests…).

            > Voyons sur un exemple simple comment changer l'implémentation de notre classe `Rectangle` sans en changer l'interface. Il s'agit ici d'un pur exercice de style, nous ne gagnerons ni en clareté, ni en efficacité.

            L'interface ne devant pas changer, nous avons donc l'obligation de ne pas modifier les signatures des méthodes (constructeur compris) de notre classe. Nous choisissons par contre de représenter un rectangle par les coordonnées de son coin supérieur gauche et ses dimensions (longueur, hauteur).

            ```py3
            class Rectangle:
                def __init__(self, x1, y1, x2, y2):
                    # On teste la validité des paramètres:

                    # Les quatre multiplications (ridicules) ci-dessous
                    # ont une double et réelle utilité:
                    # - elles déclenchent une erreur si les paramètres
                    #   ne sont pas numériques (très bien !)
                    # - elles assurent que les paramètres soient des
                    #   flottants (un type numérique adapté à notre classe)
                    x1 = 1.0*x1
                    y1 = 1.0*y2
                    x2 = 1.0*x2
                    y2 = 1.0*y2

                    if x1 > x2:
                        # Les abscisses sont dans le mauvais ordre:
                        # on les échange
                        x1, x2 = x2, x1

                    if y1 > y2:
                        # Les ordonnées sont dans le mauvais ordre:
                        # on les échange
                        y1, y2 = y2, y1

                    self.__x1 = x1
                    self.__y1 = y1
                    self.__longueur = x2 - x1
                    self.__hauteur = y2 - y1

                def x1(self):
                    return self.__x1

                def y1(self):
                    return self.__y1

                def x2(self):
                    return self.__x1 + self.__longueur

                def y2(self):
                    return self.__y1 + self.__hauteur

                def longueur(self):
                    return self.__longueur

                def hauteur(self):
                    return self.__largeur

                def translation(self, dx, dy):
                    self.__x1 += dx
                    self.__x2 += dx

                def perimètre(self):
                    return 2*(self.hauteur() + self.longueur())

                def aire(self):
                    return self.hauteur()*self.longueur()
            ```

            On peut constater que certaines méthodes sont plus simples, d'autres légèrement plus compliquées.  
            Les deux dernières méthodes n'ont strictement pas changé, puisqu'elles utilisent exclusivement l'interface publique de la classe et non pas les attributs privés.

            !!! attention "Point important"
                Le point important est que cette nouvelle implémentation de la classe `Rectangle` est parfaitement interchangeable avec la précédente.



    !!! example "Vocabulaire"

        1. En POO, comment nomme-t-on  ?

            - les nouveaux types (structures) de données que l'on définit : ...
            
            - les représentants de ces nouveaux types : ...
                
            - les variables (caractéristiques) liées à ces nouveaux types : ...
            
            - les fonctions (comportement) propres à ces nouveaux types : ...
            
            
        2. Qu'est-ce que ?
            
            - l'encapsulation :
            
            - un constructeur :
            
            - un accesseur :
            
            - un mutateur :

    ??? bug "Quelques définitions"
        - Une **classe** est une structure de données abstraite regroupant :
            - les caractéristiques de ces données, caractéristiques appellées les **attributs** ou variable d'instance;
            - les actions applicables sur les données, actions appellées les **méthodes**.
        
        - Un **objet** est un élément issu d'une classe. On parle aussi d'**instance** de la classe.
        - La création d'un objet d'une classe s'appelle une **instanciation** de cette classe. 
        - Un **constructeur** est une méthode qui permet l'instanciation. Cette méthode **initialise** (on dit aussi instancie) **l'ensemble des attributs** de l'objet. 
        - L'**encapsulation** est un principe qui consiste à regrouper des données avec un ensemble de méthodes permettant de les lire ou de les manipuler dans le but de cacher ou de protéger certaines de ces données. 
        - Les méthodes et données internes (celles plus ou moins "cacher" à l'utilisateur) sont dites **privées**.  
        - Les méthodes et données accessibles à tout utilisateur (celles que les utilisateurs de la classe connaissent) sont dites **publiques**. 

    ??? bug "Quelques remarques"
        - On définit une classe en suivant la syntaxe `#!python class NomClasse:` (nom en *CamelCase*).  
        - Les méthodes se définissent comme des fonctions, avec le mot clé `def`, sauf qu'elles se trouvent dans le corps de la classe.  
        - On construit une instance de classe grâce à son constructeur, une méthode appelée `#!python __init__`.  
        - Les méthodes prennent en premier paramètre `self`, l'instance de l'objet manipulé.  
        - On définit les attributs d'une instance dans le constructeur de sa classe, en suivant cette syntaxe : `#!python self.nom_attribut = valeur`.  
        - On peut accéder aux attribut d'un objet avec l'opérateur `.` en suivant cette syntaxe : `#!python valeur = nom_objet.nom_attribut` .
        - L'encapsulation est un principe qui consiste à cacher ou protéger certaines données des objets.  
        - Un attribut ou une méthode peut être :  
          **public** : c'est-à-dire accessible à tout utilisateur ;  
          **privé** : c'est-à-dire accessible "seulement" dans le code de la classe.  
        - Un attribut ou une méthode privée s'obtient avec deux soulignements `__` en suivant cette syntaxe : `#!python __nom_attribut` ou `#!python __nom_methode()`  
        - Un accesseur est une méthode de la classe qui retourne la valeur d'un attribut d'un objet.  
          Un accesseur est défini dans la classe, par exemple en suivant cette syntaxe : `#!python def get_attribut(self):`  
        - Un mutateur est une méthode de la classe qui modifie la valeur d'un attribut d'un objet.  
          Un mutateur est défini dans la classe, par exemple en suivant cette syntaxe : `#!python def set_attribut(self,nouvelle_valeur):`

=== "Applications et exemples"
    {{ titre_activite("Applications et exemples",[]) }}
    > La programmation orientée objet (P.O.O.) est un paradigme de programmation permettant au développeur de dépasser les objets proposés par le langage de programmation afin d'en créer de nouveaux adaptés au problème qu'il tente de résoudre.
    
    === "Création d'une classe et instanciation"
        En `Python`, on créé une classe avec la mot clé `class` qu'on nomme par habitude avec un nom en [UpperCamelCase](https://fr.wikipedia.org/wiki/Camel_case){ : target="_blank" }.

        ```py3
        class ClasseDeLycee:
            nom = "Terminale"
            numero = 1
            # liste des élèves
            eleves = []
        ```
        L'objet `ClasseDeLycee` est une sorte de «patron» à partir duquel on va pouvoir créer des objets à la demande en créant ce que l'on appelle des instances par appel de la classe.  

        ```py3
        ## On crée deux instances de l'objet ClasseDeLycee
        >>> term1 = ClasseDeLycee()
        >>> term2 = ClasseDeLycee()
        ```

        !!! aide "Attributs et méthodes"
            En POO, les objets sont décrits dans des classes contenant:  
            - des **attributs** qui sont les **données** associées à l'objet;
            - des **méthodes** qui sont les **fonctions** s'appliquant sur cet objet.
        
        En Python, on accède aux attributs et aux méthodes grâce à la notation pointée `#!python objet.attribut` ou `#!python objet.methode(...)`,  
        vous l'avez déjà utilisée car `Python` est un langage fortement orienté objet.

        ```py3
        # nos deux instances contiennent les mêmes attributs nom et numero
        >>> term1.nom
          'Terminale'
        >>> term2.nom
          'Terminale'
        >>> term1.numero, term2.numero
          (1, 2)
        ```

        L'attribut de classe `numero` n'est pas propre à une instance mais bien à toute la classe.  
        Il peut être utile dans certains cas de figures, d'avoir des attributs de classe, quand tous les objets doivent partager certaines données identiques.

        !!! bug "Attributs de classe vs attributs d'instance"
            Dans l'exemple précédent, nous avons utilisé des attributs de classe sans le `self` afin de  créer  des  attributs 
            propres non plus à chaque objet mais à la classe elle-même : on parle alors d'**attributs de classe**.
            
            `numero` est un attribut de la classe `ClasseDeLycee`.  
            À chaque fois qu'on crée un objet de type `ClasseDeLycee`, l'attribut de classe `numero`  s'incrémente de $1$.
            

        !!! aide "Les méthodes et la variable `self`"
            Les méthodes sont des fonctions définies au sein de la classe qui s'appliquent aux objets créés grâce à cette classe.  
            Une méthode prend toujours en premier paramètre l'objet lui-même par l'intermédiare du paramètre qu'on appelle par convention `self`.  
            De façon générale, le mot clé `self` désignera l'instance de l'objet au sein du code de la classe.

        ```py3
        class ClasseDeLycee:
            nom = "Terminale"
            numero = 1
            eleves = []

            def ajoute_eleve(self, eleve):
                """Cette méthode ajoute un élève dans la classe"""
                self.eleves.append(eleve)
        ```

        ```py3
        >>> term1.ajoute_eleve("Alan Turing")   # pour modifier l'attribut eleves 
        >>> term1.eleves
          ['Alan Turing']
        >>> term2.eleves # mais pas de modif pour l'instance term2
          []
        ```

        !!! aide "Méthode particulière : Initialisation avec le constructeur `__init__()`"
            Il est souvent interressant de créer des objets différents à partir d'un même classe, il est donc possible d'ajouter des arguments qui seront pris en charge lors de l'instanciation de l'objet en utilisant la méthode prédéfinie: `__init__()`.

        Voici comment nous pourrions permettre de personnnaliser notre classe dès sa création.

        ```py3
        class ClasseDeLycee:
            def __init__(self, nom, numero, eleves):
                self.nom = nom
                self.numero = numero
                self.eleves = eleves
        ```

        ```py3
        >>> prem7 = ClasseDeLycee("Première", 7, ["Ada Lovelace"])
        >>> print(prem7.nom, prem7.numero, prem7.eleves)
          Première 7 ['Ada Lovelace']
        ```
        On peut donner des valeurs par défaut aux paramètres dans la signature de la méthode `__init__`.

        ```py3
        class ClasseDeLycee:
            def __init__(self, nom="Terminale", numero=3, eleves=[]):
                self.nom = nom
                self.numero = numero
                self.eleves = eleves
        ```
        ```py3
        >>> term3 = ClasseDeLycee()
        >>> print(term3.nom, term3.numero, term3.eleves)
          Terminale 3 []
        >>> term4 = ClasseDeLycee(numero=4) # on peut personnaliser les attributs souhaités de l'instance
          Terminale 4 []
        ```

        Pour l'instant si on affiche notre instance, on a:
        
        ```py3
        >>> print(term4)
          <__main__.ClasseDeLycee object at 0x7fe037f4bc10>
        ```

        !!! aide "Méthode particulière : La méthode `__str__()`"
            Cette méthode est utilisée pour donner une représentation des objets sous forme lisible lors d'un appel de la fonction `print`.

        ```py3
        class ClasseDeLycee:
            def __init__(self, nom="Terminale", numero=3, eleves=[]):
                self.nom = nom
                self.numero = numero
                self.eleves = eleves

            def __str__(self):
                return f"<Classe de Lycée {self.nom}{self.numero}>"
        ```

        ```py3
        >>> print(term4)
          <Classe de Lycée Terminale4>
        ```

        !!! aide "Méthodes particulières : Les getters et setters"
            Il est fortement déconseillé de récupérer (`get`) ou modifier (`set`) des attributs de l'objet directement par l'utilisation de la notation pointée vue précédemment.  
            Pour chaque attribut, il est conseillé et c'est la pratique couramment recommandée de définir deux méthodes:  
            - `#!python get_attribut` : pour le récupérer.  
            - `#!python set_attribut` : pour le modifier.

        Voici ce que cela donnerait dans notre cas, on a trois attributs, il faut donc ajouter six méthodes.

        ```py3
        class ClasseDeLycee:
            def __init__(self, nom="Terminale", numero=3, eleves=[]):
                self.nom = nom
                self.numero = numero
                self.eleves = eleves

            def get_nom(self):
                return self.nom

            def set_nom(self, nom):
                self.nom = nom

            def get_numero(self):
                return self.numero

            def set_numero(self, numero):
                self.numero = numero

            def get_eleves(self):
                return self.eleves

            def set_eleves(self, eleves):
                self.eleves = eleves

            def __str__(self):
                return f"<Classe de Lycée {self.nom}{self.numero}>"
        ```

        ```py3
        >>> c = ClasseDeLycee()
        >>> print("Au début ",c)
          Au début <Classe de Lycée Terminale3>
        # Modification des attributs avec les setters
        >>> c.set_nom("Seconde")
        >>> c.set_numero(15)
        >>> print(c)
          <Classe de Lycée Seconde15>
        # Récupération des attributs avec les getters
        >>> c.get_nom(), c.get_eleves()
          ('Seconde', [])
        ```

        Il reste à définir les méthodes et attributs propres à cette classe: `voeux_parcoursup`, `mention_bac`...

    === "Personnages de JDR/MMORPG"
        !!! note "Prenons un JDR ou MMORPG lambda, dont les personnages sont constitués ainsi :"       
                * ils ont 4 caractéristiques numériques entre $1$ et $40$, à savoir `force, endurance, rapidité, intelligence` ;
                * ils ont un nombre de points de vie de départ calculé à partir des caractéristiques `endurance + force//2` ;
                * ils possèdent bien sûr un `nom` ;
                * leur nombre de `points d'expérience` au départ est de $0$, et leur `niveau` est de $1$ ;
                * Chaque personnage peut **mener une attaque**, qui consiste à ajouter un nombre aléatoire entre 1 et 20 à sa force ;
                * Chaque personnage peut **se défendre d'une attaque** qui lui est lancée
                en ajoutant un nombre aléatoire de 1 à 20 à son endurance. Si ce résultat est supérieur ou égal au 
                niveau d'attaque, l'attaque a échouée, sinon le personnage perd un nombre de points 
                de vie  égal à la différence entre le niveau d'attaque et le niveau de défense.
                
        Pour construire un tel personnage, on va devoir renseigner son nom, 
        et ses 4 caractéristiques. Ses PV, Pex et son niveau sont automatiquement calculés.
        Tous sont cependant des **attributs** du personnage.           

        !!! info inline end
            On va donc décrire un personnage par l'intermédiaire de l'interface suivante :
        
            ![perso](images/Perso1.png){: .center .imagepng}


        ??? tips "Décrire une classe en Python"
            Le code suivant permet de définir une nouvelle classe d'objets de type  `Personnage` :
            
            ``` python
            class Personnage :
                """ une classe pour représenter un personnage générique du MMORPG """
                def __init__(self, nom, force, endurance, rapidite, intelligence) :
                    self.nom = nom
                    self.force = force
                    self.endurance = endurance
                    self.rapidite = rapidite
                    self.intelligence = intelligence
                    self.pv = self.endurance + self.force//2
                    self.pex = 0
                    self.niveau = 1
            ```
            
            La définition d'une classe d'objet est effectuée par le mot-clé `class`,
            suivi du nom de la classe (commençant par une majuscule par convention) et du symbole `:`  
            Comme toujours en `Python` le symbole `:` déclenche l'attente d'un bloc indenté, qui 
            correspondra à la définition de l'objet.
            
            On trouve ensuite une `docstring` décrivant la classe, puis la définition 
            d'une `méthode` de la classe
            
            !!! aide "la méthode `__init__()`, appelée **méthode constructeur de la classe**"         
                Cette méthode prend plusieurs paramètres :
                
                1. le paramètre `self`, **obligatoire**, qui représente *'l'instance* de l'objet créé.
                2. une série de paramètres qui correspondent aux paramètres définis dans l'interface
                de l'objet `Personnage`.
            
            Puis dans cette méthode, on affecte aux **attributs de l'objet** les valeurs passées en argument, puis on calcule l'**attribut** `pv` à partir des valeurs déjà connues.   
            Enfin on défini les deux **attributs** de valeurs fixes.
            
        !!! info "Un oubli ?"
            Attention ! Ici nous ne respectons pas vraiment l'interface, puisque nous ne vérifions
            pas que les attributs passés en argument sont bien des entiers entre 1 et 40 ! Ce problème sera réglé plus tard.

            
        ??? tips "Instanciation d'objets"
            Pour créer des personnages, il suffit maintenant d'utiliser une expression de la forme : `Personnage( nom, f, e, r, i)`.  
            Notez que l'argument `self` n'est pas renseigné ! On appelle cet argument un **argument implicite**.
        
        !!! info  inline end
            On peut schématiser avec le dessin suivant :
            
            ![perso](images/Perso2.png){: .center .imagepng}

        Ainsi pour créer un objet de type `Personnage` nommé Bob, et ayant les attributs $20$ en force, $25$ en endurance, $10$ en rapidité et $30$ en intelligence, on utilise l'instruction suivante :


        ```py3
        >>> Personnage("Bob", 20, 25, 10, 30)
          <__main__.Personnage object at 0x7fb674844e48>
        ```

        On constate donc bien qu'un objet de type `Personnage` est crée. Bien sûr, l'objet n'étant pas affecté à un nom, il est immédiatement nettoyé par le *garbage collector*.  
        On crée donc une variable `playerBob` comme référence à l'objet :

        ```py3
            >>> playerBob = Personnage("Bob", 20, 25, 10, 30)
        ```
            
        L'appel au nom de la classe `Personnage` fait en réalité appel à la **méthode constructeur**, qui va permettre de créer un nouvel objet de type `Personnage`. On peut le vérifier avec la ligne suivante :

        ```py3
        >>> type(playerBob)
          <class '__main__.Personnage'>
        ```
            
        ??? tips "Accéder aux attributs et les modifier"
            Pour accéder à l'attribut `pv` de l'objet `playerBob`, il suffit d'utiliser la notation

            ```py3
            >>> playerBob.pv
              35
            ```
        
        !!! info  inline end
            Cette seconde **instance** de type `Personnage` possède aussi ses propres attributs, comme montré dans le schéma suivant :

            ![perso3](images/Perso3.png){: .center .imagepng}

        Il devient dès lors possible (mais non conseillé) de modifier la valeur d'un attribut comme lors de toute modification classique des variables :

        ```py3
        >>> playerBob.force = 18
        >>> playerBob.force
          18
        >>> playerBob.rapidite = playerBob.rapidite + 2
        >>> playerBob.rapidite
          12
        ```

        !!! tips "Deux objets"
            Nous souhaitons maintenant créer un deuxième personnage du nom de Bill :

            ```py3
            >>> playerBill = Personnage('Bill', 34, 10, 20, 12)
            ```


        !!! bug "Espace de nommage"
            Chaque **instance** d'objet possède son propre espace de nommage. 
            
            Ici même si les deux objets de type `Personnage` ont le même nom d'attribut `force`, ils ne représentent pas le même objet.
            

        ??? tips "Définitions de méthodes"
            Essayons maintenant d'afficher une chaîne de caractères nous donnant toutes les caractéristiques d'un personnage.
                
            >Test de `print`

            Essayons d'abord avec la fonction *built-in* `print()` :

            ```py3
            >>> print(playerBob)
              <__main__.Personnage object at 0x7fb6748590f0>
            ```

            C'est peu parlant !  La fonction `print` ne renvoie que l'adresse mémoire et le type de l'objet que nous venons de lui passer.

            !!!abstract "Afficher les personnage"
                Nous allons donc devoir améliorer cet affichage, en construisant notre propre **méthode**, que nous nommerons `affiche`.  
                Cette méthode devra avoir le comportement suivant :

                ```py3
                >>> playerBob.affiche()
                  "Bonjour, je suis Bob, de niveau 1. J'ai 18 en force, 25 en endurance, 12 en rapidité et 30 en intelligence. J'ai 35 Points de Vie"
                ```
                    
            !!! tips "Méthodes et attributs"

                Si les **attributs** d'une classe sont comme des **variables** spécifiques à une classe, les **méthodes** sont des **fonctions** :  
                elles peuvent prendre ou non des arguments, et ont des valeurs de retour (qui peuvent être parfois implicites : la méthode `__init__` renvoie le nouvel objet créé.)

            Pour créer cette méthode, nous allons compléter la classe `Personnage` de la manière suivante :

            ```py3
            class Personnage :
                """ une classe pour représenter un personnage générique du MMORPG """
                def __init__(self, nom, force, endurance, rapidite, intelligence) :
                    self.nom = nom
                    ...
                    
                    
                def affiche(self) :
                    print(f"Bonjour, je suis {self.nom}, de niveau {self.niveau}."
                    f"J'ai {self.force} en force, {self.endurance} en endurance, {self.rapidite}"
                    f" en rapidité et {self.intelligence} en intelligence. J'ai {self.pv} Points de Vie")
            ```

            Vous constatez que :

            1. Dans la construction de la méthode `affiche`, apparaît l'**argument implicite** `self`, qu'il est impératif d'utiliser pour avoir accès aux **attributs** de l'objet.
            2. Dans l'appel de la méthode **aucun argument n'est passé**.

            En rechargeant le module, puis en recréant les objets `playerBill` puis `playerBob`, on obtient alors les affichages suivants :

            ```py3
            >>> playerBob.affiche()
              Bonjour, je suis Bob, de niveau 1.J'ai 18 en force, 25 en endurance, 12 en rapidité et 30 en intelligence. J'ai 34 Points de Vie
            >>> playerBill.affiche()
              Bonjour, je suis Bill, de niveau 1.J'ai 34 en force, 10 en endurance, 20 en rapidité et 12 en intelligence. J'ai 27 Points de Vie
            ```

            !!! question "Implémenter la méthode `attaque`"

                Dans notre interface de départ, nous avions prévu une méthode `attaque()` qui renvoie un entier égal à la valeur 
                de force du personnage auquel on ajoute un nombre aléatoire entre $1$ et $20$.
                
                Comment implémenter une telle méthode ?
                
            ??? done "Une implémentation possible"

                ```py3
                from random import randint 

                class Personnage :
                    """ une classe pour représenter un personnage générique du MMORPG """
                    def __init__(self, nom, force, endurance, rapidite, intelligence) :
                        self.nom = nom
                        ...
                        
                        
                    def affiche(self) :
                        ...
                        
                    def attaque(self) :
                        return self.force + randint(1,20)
                ```
                
                Cette méthode peut alors être utilisée comme dans les exemples ci-dessous :

                ```py3
                >>> playerBob.attaque()
                  22
                >>> playerBob.attaque()
                  27
                >>> playerBill.attaque()
                  38
                ```

            !!! question "Implémenter la méthode `defense(valeurAttaque)`"
                Dans notre interface de départ, nous avions prévu une méthode `defense(valeurAttaque)` qui ajoute un nombre aléatoire de $1$ à $20$ à l'endurance du personnage.  
                Si ce résultat est supérieur ou égal au niveau d'attaque, l'attaque a échouée et la méthode renvoie `True`.  
                Sinon le personnage perd un nombre de points de vie  égal à la différence entre le niveau d'attaque et le niveau de défense, et la méthode renvoie `False`.
                
                Comment implémenter une telle méthode ?
                
            ??? done "Une implémentation possible"

                ```py3
                from random import randint 

                class Personnage :
                    """ une classe pour représenter un personnage générique du MMORPG """
                    def __init__(self, nom, force, endurance, rapidite, intelligence) :
                        self.nom = nom
                        ...
                        
                        
                    def affiche(self) :
                        ...
                        
                    def attaque(self) :
                        return self.force + randint(1,20)
                        
                    def defense(self, valeurAttaque) :
                        valeurDefense = self.endurance + randint(1,20)
                        if valeurAttaque> valeurDefense :
                            self.pv -=  valeurAttaque-valeurDefense
                            return False
                        else : return True
                ```
                
                cette méthode peut alors être utilisée ainsi :

                ```py3                
                >>> playerBill.defense(playerBob.attaque())
                  False
                >>> playerBill.pv
                  18
                ```
            

        ??? tips "Méthodes spécifiques"

            !!! info inline end 

                La liste de toutes les méthodes d'une classe, y compris des **DUNDERS**,
                peut-être obtenue par l'intermédiaire de la commande suivante :

                ```py3
                >>> dir(Personnage)
                  ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__',
                  '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__',
                  '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__',
                  '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__',
                  '__repr__', '__setattr__', '__sizeof__', '__str__',
                  '__subclasshook__', '__weakref__',
                  'affiche', 'attaque', 'defense']
                ```

            !!! info "DUNDERS"
                Il existe plusieurs méthodes spécifiques définies automatiquement dès qu'on crée une classe d'objets.  
                Ces méthodes sont toutes  de la forme `__truc__()` (c'est-à-dire que le nom de la classe est préfixé par un double tiret du bas, soit *Double UNDERScore*, ce qui a donné le nom de méthodes **DUNDERS**).

                Ce sont des méthodes universelles que possèdent toute classe en `Python`, et qui permettent de gérer un certain nombre d'actions.  
                Par exemple l'instruction `Personnage('Bob', 18, 25, 12, 30)` fait appel à la méthode DUNDERS `__init__()` que nous avons définie.
                
                > Il est ainsi possible de redéfinir un certain nombre de ces méthodes selon nos utilisations.
                
                Le tableau ci-dessous vous présente quelques-uns de ces DUNDERS, applicables à des objets `t` et `other` instances de la classe :
                
                | méthode | Appel | Intérêt |
                |:---: | :--- | :--- |
                | `__str__(self)` | `str(t)` | renvoie une chaîne de caractères décrivant l'objet `t` |
                | `__lt__(self,other)` | `t < other` | permet de définir la relation *plus petit que* entre deux objets, renvoie `True` ou `False` selon la définition proposée |
                | `__len__(self)` | `len(t)` | permet de définir la longueur de l'objet `t` |
                | `__contains__(self,x)` | `x in t` | permet de définir l'appartenance de `x` à `t` |
                | `__eq__(self, other)` | `t == other` | permet de définir l'égalité entre deux objets `t` et `other` |
                | `__add__(self, other)` | `t + other` | définit l'addition de deux objets `t` et `other` |
                | `__mul__(self, other)` | `t * other` | définit la multiplication de deux objets `t` et `other` |

            !!! example "redéfinition de la méthode `__str__(self)`"
                Il est assez facile de redéfinir la méthode `__str__(self)`, puisque nous avons déjà une chaîne de caractère qui nous convient : celle de la méthode `affiche(self)`.  
                Nous allons alors changer la méthode `affiche(self)` qui renverra la chaîne de caractère générée par la méthode `__str__(self)` (pour des raisons pratiques, la chaîne sera multi-ligne):

                ```py3
                from random import randint 

                class Personnage :
                    """ une classe pour représenter un personnage générique du MMORPG """
                    def __init__(self, nom, force, endurance, rapidite, intelligence) :
                        self.nom = nom
                        ...
                        
                    def __str__(self) :
                        return  f"""Bonjour, je suis {self.nom}, de niveau {self.niveau}.
                    J'ai {self.force} en force, {self.endurance} en endurance, {self.rapidite} en rapidité
                    et {self.intelligence} en intelligence. J'ai {self.pv} Points de Vie"""
                        
                    def affiche(self) :
                        print(str(self))
                        
                    ...
                ```
                On a alors la possibilité d'utiliser les commandes suivantes :

                ```py3
                >>> str(playerBob)
                  "Bonjour, je suis Bob, de niveau 1.\n         J'ai 18 en force, 25 en endurance, 12 en rapidité\n           et 30 en intelligence. J'ai 34 Points de Vie"
                >>> playerBob.affiche()
                  Bonjour, je suis Bob, de niveau 1.
                  J'ai 18 en force, 25 en endurance, 12 en rapidité
                  et 30 en intelligence. J'ai 34 Points de Vie
                ```

        !!! question "Définir l'égalité entre deux personnages"
            
            On considère que deux personnages sont égaux quand ils possèdent les mêmes caractéristiques numériques de bases (`force`, `endurance`, `rapidite` et `intelligence`).
            
            Redéfinir  la méthode DUNDERS `__eq__(self, u)` pour qu'elle corresponde à cette définition.
            
        ??? done "Solution"

            ```py3
            from random import randint 

            class Personnage :
                """ une classe pour représenter un personnage générique du MMORPG """
                def __init__(self, nom, force, endurance, rapidite, intelligence) :
                    self.nom = nom
                    ...
                    
                def __eq__(self, other) :
                    return (self.force == other.force) and (self.endurance == other.endurance) and (self. rapidite == other.rapidite) and (self.intelligence == other.intelligence)
                    
                ...
            ```
            
            On a alors l'utilisation :

            ```py3
            >>> playerBob == playerBill
              False
            >>> playerBob == Personnage('Marty', 18, 25, 12, 30)
              True
            ```

        ??? question "Fight !"
            Un combat de ce MMORPG se déroule selon le schéma suivant :
            
            1. Chaque personnage tire son *initiative* en ajoutant un nombre aléatoire entre $1$ et $20$ à sa valeur de rapidite.
            2. Le joueur ayant l'initiative la plus élevée effectue son attaque en premier, et le second se défend.  
               En cas d'égalité d'initiative, le joeur d'attaque sera celui qui possède la rapidité la plus élevée.  
               En cas d'égalité de rapidité, le premier joueur sera déterminé aléatoirement.
            3. Si le deuxième joueur est toujours vivant (ses points de vie sont supérieurs à 0), il effectue son attaque, et le premier se défend.
            4. Si les deux joueurs sont toujours vivants, on recommence un nouveau tour en reprenant en 1. Sinon on affiche le vainqueur.
            5. Le vainqueur récolte un nombre de points d'expérience égal à :
                
                nombre d'attaque réussie * 2 + nombre de defense réussie
            
            Vous devrez implémenter un programme simulant un combat entre `Bob` et `Bill`, dont la sortie console sera sous la forme suivante :
            
                Round 1
                Bob a l'initiative et attaque avec 28
                Bill réussit sa défense
                Bill attaque avec 40
                Bob rate sa défense et n'a plus que 25 points de vie
                
                Round 2
                ...
                
                
                Round n
                Le vainqueur est ..., il lui reste ... points de vie. Il gagne ... points d'expériences.
            
            Pour réaliser ce programme, vous devrez :
            
            1. Compléter le fichier `personnage.py` contenant la classe `Personnage` de la manière suivante :
            
                1. La méthode constructeur `__init__()` devra lever des exceptions de type `TypeError` explicites, si `nom` n'est pas de type `str`, ou bien si les 4 autres attributs de construction ne sont pas de type `int`.  
                   **L'erreur devra interrompre l'exécution du programme qui la déclenche !** 

                2. La méthode constructeur `__init__()` devra lever des exceptions de type `ValueError` explicites si les valeurs fournies pour les 4 attributs numériques ne sont pas entre $1$ et $40$.  
                   ** L'erreur devra interrompre l'exécution du programme qui la déclenche !**  

                3. La méthode constructeur `__init__()` devra lever des exceptions de type `ValueError` explicites si la chaîne de caractère `nom` est vide.  
                   ** L'erreur devra interrompre l'exécution du programme qui la déclenche !**  
                
            2. Vous devrez pour réussir construire une méthode supplémentaire pour la classe `Personnage` : la méthode `initiative(self)` qui renvoie un entier représentant le score d'initiative du personnage.  
            3. Le programme permettant le combat devra être dans un fichier `combat.py` séparé de celui contenant la classe `Personnage` (la classe `Personnage` étant importée depuis le module `personnage.py` par la commande suivante :
            
                `#!python from personnage import Personnage`
            
            4. Un fichier compressé `.zip` contenant les deux fichiers (celui du programme et le module contenant la classe `Personnage`) sera rendu via Moodle.


## Cours

Vous pouvez télécharger une copie au format pdf du diaporama de synthèse de cours présenté en classe :

<span class='centre'>[Diaporama de cours :fontawesome-regular-file-pdf:](https://lgt-beaussier-83512-moodle.atrium-sud.fr/mod/resource/view.php?id=15977){.md-button}</span>
!!! warning "Attention"
    Ce diaporama ne vous donne que quelques points de repères lors de vos révisions. Il devrait être complété par la relecture attentive de vos **propres** notes de cours et par une révision approfondie des exercices.

> pour aller plus loin : [Avoir la classe avec les objets](https://python.sdv.univ-paris-diderot.fr/19_avoir_la_classe_avec_les_objets/=){ : target="_blank" }.




## Exercices

<!-- {{ exo("Définir et instancier une classe",[],0) }} -->
[Accessibles sur Moodle](https://lgt-beaussier-83512-moodle.atrium-sud.fr/course/view.php?id=837#section-2){ : target="_blank" }.