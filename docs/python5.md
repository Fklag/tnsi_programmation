# Les listes, les piles et les files

![BO2](assets/images/BO2.png){: .center .width=50% }

## Listes

!!! info "Une liste est une collection finie d'éléments qui se suivent, qu'il est possible de manipuler individuellement."
    C'est donc une structure de données **linéaire**.

    Une liste `L` est composée de 2 parties : sa tête (historiquement notée $\texttt{car}$), qui correspond au dernier élément ajouté à la liste, et sa queue ( notée $\texttt{cdr}$) qui correspond au reste de la liste. Le langage de programmation `Lisp` (*inventé par John McCarthy en 1958*) a été un des premiers langages de programmation à introduire cette notion de liste (`Lisp` signifie "list processing").
    
=== "Le type abstrait  **Liste**"
    !!! warning "Primitives"

        Le type abstrait  **Liste**  peut être défini par l'*interface* suivante contenant 5 opérations primitives :

        - **Des constructeurs :**
            - `#!python listevide()`  pour construire une liste vide
            - `#!python construit(e, L)`  pour construire une nouvelle liste contenant un premier élément  `e`  (sa tête)  
               et une suite `L`  (sa queue, qui est une liste). Cet opérateur est aussi souvent noté  `cons` .
        - **Des sélecteurs :**
            - `#!python premier(L)`  pour accéder au premier élément de la liste  `L` , sa tête. Cet opérateur est aussi souvent noté  $\texttt{car}$.
            - `#!python reste(L)`  pour accéder au reste de la liste  `L`  c'est-à-dire sa queue. Cet opérateur est aussi souvent noté  $\texttt{cdr}$.
        - **Un prédicat :**
            - `#!python estvide(L)`  pour tester si une liste est vide.

        On sait depuis les travaux de Mac Carthy sur le langage `Lisp`, qu'avec ces 5 opérations on peut reconstruire toutes les opérations sur les listes (accéder à un élément, modifier un élément, ajouter/supprimer un élément, calculer la longueur, tester l'appartenance, etc.)

        Voici les principales opérations qui peuvent être effectuées sur une liste :

        | Actions |    Instruction   |
        |--------------------|:------:|
        | **créer** une liste `L` vide | `#!python L=vide()`  |
        | **Tester** si la liste `L` est vide  | `#!python estVide(L)`  |
        | **Ajouter** un élément `x` en tête de la liste `L` | `#!python ajouteEnTete(x,L)` |
        | **Supprimer** la tête `x` d'une liste `L` et renvoyer cette tête `x` | `#!python supprEnTete(L)` |
        | **Compter** le nombre d'éléments dans une liste`x` | `#!python compte(L)` |
        | **Créer** une nouvelle liste `L1` à partir d'un élément `x` et d'une liste existante `L` | `#!python L1 = cons(x,L)` |

=== "Exemples :"
        
    !!! example "Voici une série d'instructions qui s'enchaînent avec des commentaires :"

        1. `#!python L=vide()` : on a créé une liste vide.
        1. `#!python estVide(L)` : renvoie vrai.
        1. `#!python ajoutEnTete(3,L)` : La liste `L` contient maintenant l'élément `3`.
        1. `#!python estVide(L)` : renvoie faux.
        1. `#!python ajoutEnTete(5,L)` : la tête de la liste `L` correspond à `5`, la queue contient l'élément `3`.
        1. `#!python ajoutEnTete(8,L)` : la tête de la liste `L` correspond à `8`, la queue contient les éléments `3` et `5`.
        1. `#!python t = supprEnTete(L)` : la variable `t` vaut `8`, la tête de `L` correspond à `5` et la queue contient l'élément `3`.
        1. `#!python  L1 = vide()`
        1. `#!python  L2 = cons(8, cons(5, cons(3, L1)))`  
            : La tête de `L2` correspond à `8` et la queue contient les éléments `5` et `3`.
        
        ![liste](assets/images/interface-liste-2.png)

=== "Une 1ère implémentation"
        
    !!! example "En utilisant des tuples pour implémenter la structure de liste"

        ![listec](assets/images/res_listes1.png)
        {{IDE('listec/liste')}}

        ???+ "À faire - Vérifier le bon fonctionnement de cette implémentation en exécutant ces instructions"
            - nil = vide()
            - afficher estVide(nil)
            - L = cons(5, cons(4, cons(3, cons(2, cons(1, cons(0,nil))))))
            - afficher tete(L)
            - afficher queue(L)
            - afficher estVide(L)
            - afficher compte(L)
            - L = ajouteEnTete(6,L)
            - afficher compte(L)
            - x, L = supprEnTete(L)
            - afficher x
            - afficher compte(L)
            - x, L = supprEnTete(L)
            - afficher x
            - afficher compte(L)
            - afficher tete(L)
            - afficher queue(L)

=== "Note"
    !!! warning "En `Python` la classe `#!python list` n'est pas une liste"
        Contrairement à ce que pourrait laisser croire son nom, la classe `#!python list` est une structure de donnée plus complexe qui cherche à concilier les avantages des tableaux et des listes chaînées, à savoir :  
        "Être une structure de donnée **dynamique** dans laquelle les éléments sont **accessibles à coût constant**".

        Ainsi, ce que la documentation de Python appelle “`list`” s'utilise comme des tableaux (au sens où Python permet l'accès aléatoire aux valeurs stockées, grâce à l'indexation) et comme une liste chaînée (au sens où le stockage est bien géré de façon **dynamique** : la taille pouvant varier en cours d'exécution d'un programme).

!!! info "Les **listes chaînées**, **piles** et **files**"
    Ce sont toutes des structures de données à une dimension permettant de stocker de manière linéaire une suite finie de données.

    ![liste](assets/images/dossiers.png)

    ???+ example "Pour s'imaginer ces structures linéaires"
        Imaginez un garagiste devant gérer les clients de la journée. Chaque rendez-vous est inscrit sur une fiche contenant toutes les informations nécessaires (*nom du client, modèle du véhicule, travaux à faire...*) qu'il devra stocker et à laquelle il devra accéder le moment venu. Devant lui s'offrent plusieurs choix pour la gestion de ces fiches.  
        Nous allons examiner les structures de **liste chaînée**, **pile** et **file** et voir comment elles peuvent s'appliquer à cet exemple.

=== "La structure liste chaînée"
    !!! warning "Liste chaînée"
        Une **liste** (ou liste chaînée) est une structure décrivant une succession linéaire de données dans laquelle il est possible de lire, d'ajouter ou de supprimer des données au début, à la fin ou à l'intérieur de la liste.  
        Les listes chaînées sont une structure de donnée très souple et efficace. Chaque enregistrement d'une liste chaînée est souvent appelé **élément** , **nœud** ou **maillon**.

        La **tête** d'une liste est son premier nœud. La **queue** d'une liste peut se référer soit au reste de la liste après la tête, soit au dernier nœud de la liste.

        Le champ de chaque nœud qui contient l'adresse du nœud suivant ou précédent est généralement appelé **lien** ou **pointeur**. Le contenu est placé dans un ou plusieurs autres champs appelés **données**, **informations** ou **valeur**.



        
        Il ne faut pas confondre cette structure de liste avec les listes `Python` qui sont en fait une structure bien plus riche, que l'on pourrait qualifier de **tableau dynamique** dans laquelle les éléments sont accessibles à coût constant.

        Dans une liste chaînée, chaque élément contient la donnée et un pointeur vers l'élément suivant. On accède à la liste via un pointeur vers son premier élément :  
        ![listec](assets/images/liste1.png){ align=right }

        ???+ example "On peut imaginer pour notre exemple du garagiste une bannette contenant toutes les fiches client de la journée."
            Notre garagiste peut les parcourir une à une, insérer au milieu une nouvelle fiche pour un client qu'il voudrait intercaler etc...
            ![bannette](assets/images/corbeille-murale.jpg){ align=left }

    === "Liste chaînée :"
        !!! example "Une liste chainée `L` est entièrement définie par son maillon de tête `L.tete`, c'est à dire l'adresse de son premier maillon."

            Chaque élément (ou maillon) $\texttt{M}$  de la liste est composé :

            - d'un **contenu** utile `M.val`  (de n'importe quel type),
            - d'un **pointeur** `M.suiv`  pointant vers l'élément *suivant* de la séquence.
            ![listech](assets/images/drawit-diagram-22.png){ align=right }

            Le dernier élément de la liste possède un pointeur `M.suiv` vide : ![listech](assets/images/drawit-diagram-17.png)


    === "Exemples d'opérations sur les listes chaînées :"
        !!! example "Voici quelques opérations possibles sur les listes chaînées :"

            Il n'existe pas de normalisation pour les **primitives** de manipulation de liste.

            - **Ajouter/retirer** un élément au début, à la fin, à l'intérieur ;
            - **Concaténer** de deux listes ;
            - **Scinder** une liste en deux ;
            - **Accéder** au $n$-ième élémént ;
            - **Rechercher** la présence d'un élément.

            ??? example "Exemple : Ajout d'un élément au début de la liste chaînée"
                ![listecn](assets/images/liste2.png){ align=right }
                ![listeca](assets/images/liste3.png){ align=right }

            ??? example "Exemple : Insertion d'un maillon"
                Pour insérer un maillon $\texttt{nM}$  après un maillon $\texttt{M}$ , il faut :

                1. Faire pointer le pointeur `nM.suiv`  vers `M.suiv`
                1. Faire pointer le pointeur `M.suiv`  vers $\texttt{nM}$ 

                ![listechn](assets/images/drawit-diagram-27.png){ align=left }
                ![listecha](assets/images/drawit-diagram-26.png){ align=right }

            ??? example "Exemple : Suppression d'un maillon"
                Pour supprimer le maillon suivant un maillon $\texttt{M}$ , il faut :

                1. Faire pointer le pointeur `nM.suiv`  vers `M.suiv.suiv`
                1. Détruire (effacer de la mémoire) le maillon `M.suiv` 

                ![listechn](assets/images/drawit-diagram-34.png){ align=left }
                ![listecha](assets/images/drawit-diagram-33.png){ align=right }

                **Remarque :** *dans certains langages disposants d'un dispositif de [ramasse-miettes](https://fr.wikipedia.org/wiki/Ramasse-miettes_(informatique)) (comme `Python` par exemple), le simple fait de ne plus être référencé détruit l'objet Maillon. Dans d'autres langages, il faut explicitement détruire l'objet, et par conséquent, en garder une référence avant l'étape 1.*

    === "Implémentation"
        !!! warning "On utilise deux classes: `Maillon` et `ListeC` :"

            ```py3
            class Maillon:
                def __init__(self):
                    self.val = None
                    self.suiv = None # Pas de maillon suivant

                def __str__(self):
                    if  self.suiv is not None:
                        return str(self.val) + "-" + str(self.suiv.val)
                    else:
                        return str(self.val) + "-" + str(self.suiv)
            ```
            Pour construire une liste, <!-- il suffirait d'appliquer le constructeur de la classe Cellule:  
            `#!python liste = Maillon(1, Maillon(2, Maillon(3, None)))` On--> on écrit <!-- toutefois--> une classe `ListeC` (à compléter).  
            Son attribut `tete`  est de type `Maillon` , ou bien vaut `None`  si la liste est vide.

            ```py3
            class ListeC:
                def __init__(self):
                    self.tete = None # Liste vide
            ```
            On peut alors créer une liste `1-2-None` ainsi :
            ```py3
            L = ListeC()
            M1, M2 = Maillon(), Maillon()
            M1.val, M1.suiv = 1, M2
            M2.val, M2.suiv = 2, None
            L.tete = M1
            ```
    === "Exercices"

        ???+ {{exercice()}}
            On implémente la fonction `#!python est_vide(L)` simplement de cette manière :
            ```py3
            def est_vide(L):
                return L.tete is None
            ```

            1. Implémenter en Python les fonctions `#!python taille(L)` et `#!python get_dernier_maillon(L)`.
            {{IDE('listec/listec')}}

            2. Implémenter en Python la fonction `#!python get_maillon_indice(L, i)`. <!-- https://info.blaisepascal.fr/nsi-listes-chainees -->  
            puis en suivant les schémas des interfaces d'insertion et de suppression de maillon, implémenter en `Python` les fonctions `#!python ajouter_debut(L, nM)` , `#!python ajouter_fin(L, nM)` 

            {{IDE('listec/listec2')}}

            On donne ci-dessous à titre d'exemple le code des fonctions `#!python ajouter_apres(L, M, nM)`, puis `#!python supprimer_debut(L)` , `#!python supprimer_fin(L)`  et `#!python supprimer_apres(L, M)` (ces dernières fonctions retournant le maillon supprimé).

            ```py3
            def ajouter_apres(L, M, nM): 
                """ Ajoute le maillon M après celui d'indice i dans la liste L """
                nM.suiv = M.suiv
                M.suiv = nM

            def supprimer_debut(L): 
                """ Supprime le 1er maillon de la liste L et le renvoie """
                t = L.tete
                L.tete = L.suiv
                return t

            def supprimer_fin(L): 
                """ Supprime le dernier maillon de la liste L et le renvoie """ 
                m = L.tete
                while m.suiv.suiv is not None:
                    m = m.suiv
                m.suiv = None
                return m

            def supprimer_apres(L, M): 
                """ Supprime le maillon de la liste L situé après le maillon M et le renvoie """ 
                s = M.suiv
                M.suiv = M.suiv.suiv
                return s
            ```

            Voici un module contenant une classe de liste chainée `ListeC`, possédant, en autres, les méthodes `#!python .est_vide()` , `#!python .ajouter_debut(m)` ,  `#!python ajouter_fin(m)` , `#!python supprimer_debut()`, $\ldots$

            ??? "Module listes_chainees.py"
                ```py3
                --8<--- "docs/scripts/listec/listes_chainees.py"
                ```

            On peut utiliser les classes de ce module dans un autre programme en utilisant l'instruction :  
            `#!python from listes_chainees import *`  

    === "Représentation d'une liste en mémoire."

        !!! note "Avantage de la liste chaînée par rapport au tableau de taille fixe ou dynamique (comme le type `list` en `Python`)."
            Le problème avec les liste chaînées est qu'on ne peut pas accéder directement à un élément (il faut parcourir la liste jusqu'à l'emplacement voulu).  
            Par contre, **on peut facilement supprimer et ajouter des éléments au milieu de la liste** (alors qu'avec un tableau, il faudrait décaler tous les éléments se trouvant après).

        ???+ {{ext()}}

            Lorsque l'implémentation de la liste fait apparaı̂tre une chaı̂ne de valeurs, chacune pointant vers la suivante, on dit que la liste est une liste chaı̂née.  
            ![listecm](assets/images/liste01.png)

            - Une liste est caractérisée par un ensemble de cellules.  <!-- Une liste chainée `L` est entièrement définie par son maillon de tête, c'est à dire l'adresse de son premier maillon.-->
            - Le lien (on dira souvent le $\textit{pointeur}$) vers la variable est un lien vers la première cellule, qui renverra elle-même sur la deuxième, etc.
            - Chaque cellule contient donc une valeur et un lien vers la cellule suivante.

            Une liste peut être vide (la liste vide est notée $\texttt{x}$ ou bien $\texttt{None}$ sur les schémas)

            ??? example "Temps d'accès non constant"
                Une conséquence de cette implémentation sous forme de liste chaînée est la non-constance du temps d'accès à un élément de liste : pour accéder au 3ème élément, il faut obligatoirement passer par les deux précédents.  
                **Dans une liste chaînée, le temps d'accès aux éléments n'est pas constant**.

                Dans une liste chaînée, il est impossible de connaître à l'avance l'adresse d'une case en particulier, à l'exception de la première. Pour accéder à la $n$-ième case, il faut donc parcourir les $n − 1$ précédentes : le coût de l'accès à une case est proportionnelle à la distance qui la sépare de la tête de la liste. 

            ??? example "Structure de donnée dynamique"
                En contrepartie, **ce type de structure  est dynamique** : une fois la liste crée, il est toujours possible de modifier un pointeur pour insérer une case supplémentaire.  
                En résumé :  
                - une liste chaînée est une structure de donnée dynamique ;
                - le $n$-ième élément d'une liste chaînée est accessible en temps proportionnel à $n$.

=== "La structure de pile"
    !!! warning "Pile"
        La structure de **pile** (ou *Stack*) est un cas particulier de liste chaînée dans lequel l'accès aux éléments se fait grâce à un **unique pointeur** vers le sommet de la pile, appelé **top**.  
        **On accède uniquement au premier élément de la pile** : celui que l'on nomme *le sommet de la pile*.  
        
        On représente en général cette structure sous forme verticale. On peut prendre pour exemple une pile d'assiettes :

        ![assiettes](assets/images/plates.png) ![LIFO](assets/images/pile1.png){ align=right }
        
        - La dernière assiette rangée sera la première que l'on ressortira ;
        - On parle parfois de pile **LIFO** (*Last In First Out* : dernier entré, premier sorti).
        

        ???+ example "On peut imaginer pour notre exemple du garagiste une pile contenant toutes les fiches client de la journée."
            Notre garagiste traite en premier la dernière fiche client posée sur le sommet de la pile...
            ![lifo](assets/images/pile.jpg){ align=left }

    === "Opérations valides sur les piles :"
        !!! example "Les opérations permises sur les piles sont plus simples que sur les listes "

            ![pile](assets/images/drawit-diagram-40.png){ align=right }

            - Insérer un élément dans la pile est appelé **empiler** ou pousser (*push*)
            - Supprimer un élément de la pile est appelé **dépiler** (*pop*)

            Vous voyez donc qu'il n'est pas possible d'accéder directement au $n$-ième élément d'une pile :  
            Il faudrait dépiler les $n-1$ premiers éléments et de ce fait, détruire partiellement notre pile.

            Il est permis : ![LIFOp](assets/images/interface-pile-1.png)
            

            - **D'empiler** un nouvel élément : `#!python push(P, e)`  empile la valeur `e`  sur la pile `P`;
            - **De dépiler** le dernier élément saisi : `#!python pop(P)`  extrait et renvoie la valeur du sommet de la pile `P`;
            - De **consulter** la valeur de l'élément au **sommet de la pile** sans le dépiler ;
            - De **tester** si la **pile est vide** : `#!python est_vide(P)`  renvoie vrai si la pile est vide;
            - De connaître le **nombre d'éléments** dans la pile.

           

    === "Implémentation en `Python`"
        !!! warning "L'implémentation des piles en python se fait facilement à l'aide des méthodes `append()` et `pop()` du type `list` :"

            - `#!python ma_pile.append(ma_valeur)   # permet d'empiler une valeur`
            - `#!python ma_pile.pop()               # permet de dépiler une valeur`
            - `#!python len(ma_pile)                # renvoie la longueur de ma_pile`

            En `Python`, les Piles ne sont pas nativement implémentées :  
            il n'existe pas de type Pile, comme il existe un type `str` ou `int` ou `tuple`...

        === "Implémentation avec `ListeC`"
            !!! example "Implémentation avec une Liste chainée"
                
                !!! bug "Définir en `Python` une classe **`Pile`**  en utilisant la notion de liste chaînée"
                    Il faudra tout d'abord définir une première classe créant un objet (un maillon) constitué de deux attributs :  

                    - une valeur  
                    - et un pointeur (vers un autre maillon)

                    ce qui nous permettra ensuite de définir une pile comme une liste chaînée de maillons.   
                    La pile vide se distinguera par son attribut égal à la constante None. 
             
                **Réaliser** l'interface complète de cette pile.

                {{IDE('listec/pile_avec_listeC')}}

        === "Implémentation avec un Tableau"
            !!! example ""
                !!! bug "Définir en `Python` une classe **`Pile`** en utilisant une *liste Python* comme attribut"
                    À l'aide de seulement 2 méthodes du type `list` et une fonction
            
                **Réaliser** l'interface complète de cette pile dont les méthodes correspondront aux opérations autorisées.

                {{IDE('listec/pile_avec_list')}}


    === "Exemples d'utilisation"
        !!! example "Les piles sont extrèmement utiles en informatique et vous les utilisez quotidiennement, parfois même sans vous en rendre compte :"

            - La fonction **annuler**  ++ctrl+"Z"++  de votre traitement de textes par exemple est une pile :  
              Quand vous tapez $\texttt{Ctrl+Z}$, vous annulez la dernière opération effectuée.  
              Quand vous faites une nouvelle opération, celle-ci est mémorisée au sommet de la pile.  
              Vous ne pouvez pas annuler l'avant dernière opération sauf à annuler la dernière.
            - Le bouton retour de votre navigateur internet fonctionne également à l'aide d'une pile.  
              Les pages web consultées lors de votre navigation sur une page sont empilées et le bouton retour permet d'accéder à la dernière page présente sur la pile.
            - Certaines calculatrices fonctionnent à l'aide du'une pile pour stocker les arguments des opérations :  
              c'est le cas de beaucoup de calculatrices de la marque $\texttt{HP}$, dont la première calculatrice scientifique ayant jamais été produite : la $\texttt{HP 35}$ de 1972.

        ??? example "Exemple de calcul avec une pile"

            Le mode de calcul avec une pile s'appelle **RPN** (*Reverse Polish Notation* ou notation polonaise inverse). 
            Dans cette logique postfixée, on saisit d'abord les arguments de l'opération puis en dernier, l'opération à réaliser.

            Exemple : Pour faire $2+3$ on empilera $2$, puis $3$ et enfin on invoquera la fonction $+$.  
            Cette logique est extrèmement efficace et rapide, en particulier dans les enchaînements d'opérations car elle ne nécessite pas de saisir des parenthèses.  
            Elle permet aussi de faire moins d'erreurs de calcul car on est obligé de réfléchir aux priorités des opérations au moment de la saisie.

            ![HP-45](assets/images/cam.png) <!-- (https://youtu.be/rXRMFTRtA4A)--> Voici une illustration en vidéo de l'utilisation de la calculatrice $\texttt{HP-45}$ (1974) qui est l'une des toutes premières caculatrices scientifiques.  
            
            
            <iframe width="560" height="315" src="https://www.youtube.com/embed/rXRMFTRtA4A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            !!! note "Test"
                - On tape sur la $\texttt{HP-45}$ la séquence de touche suivante : $\texttt{12 ENTER 4 ENTER 3 x -}$.  
                  Quel est le calcul effectué ? Quel est le résultat sur le sommet de la pile ?
                - On souhaite effectuer le calcul $\texttt{(12−4)×3}$ . Quelle séquence de touche doit-on révoir ?

        ??? example "Remarque :"
            En informatique, un dépassement de pile ou débordement de pile (en anglais, *stack overflow*) est un bug causé par un processus qui, lors de l'écriture dans une pile, écrit à l'extérieur de l'espace alloué à la pile, 
            écrasant ainsi des informations nécessaires au processus. *Stack Overflow* est aussi connu comme étant un site web proposant des questions et réponses sous la forme d'un forum d'entre-aide sur un large choix de thèmes concernant 
            la programmation informatique. Il y a de forte chance que vous trouviez la réponse à un problème informatique même ardu sur *Stack Overflow* !

=== "La structure de file"
    !!! warning "File"
        Dans une **file** (ou *Queue*) les éléments sont placés les uns à cotés des autres comme dans une pile, à la différence que l'on sort les éléments du plus ancien vers le plus récent.  

        ![FIFO](assets/images/enqueue.jpg) 

        L'accès aux objets de la file se fait grâce à **deux pointeurs**, l'un pointant sur l'élément qui a été inséré en premier et l'autre pointant sur celui inséré en dernier.

        ![pile](assets/images/drawit-diagram-41.png)



        ???+ example "On peut imaginer pour notre exemple du garagiste un organisateur contenant toutes les fiches client de la journée."
            Il reçoit les clients le matin et réceptionne leurs fiches dans une file d'attente. Pour effectuer les travaux sur le véhicule, le garagiste va récupérer les fiches en commençant par le premier client arrivé, c'est à dire la première fiche entrée dans la file. C'est la loi du premier arrivé, premier servi ou **FIFO** (*First In First Out*). C'est le comportement classique d'une file d'attente.
            ![lifo](assets/images/organisateur.jpg){ align=left }

    === "Opérations valides sur les files :"
        !!! example "Les opérations permises sur les files sont similaires aux piles "

            - Insérer un élément dans la file est appelé **enfiler**  *(enqueue*)
            - Supprimer un élément de la file est appelé **défiler** (*dequeue*)

            Vous voyez donc qu'il n'est pas possible d'accéder directement au $n$-ième élément d'une file :  
            Il faudrait dépfiler les $n-1$ premiers éléments et de ce fait, détruire partiellement notre file.

            Il est permis : ![LIFOf](assets/images/interface-file.png)
            

            - **D'enfiler** un nouvel élément : `#!python enfiler(F, e)`  ajoute l'élément `e` à la queue de la file `F`;
            - **De défiler** le dernier élément saisi : `#!python defiler(F)`  extrait et renvoie l'élément de la tête de la file `F`;
            - De **tester** si la **file est vide** : `#!python est_vide(F)`  renvoie vrai si la file est vide;
            - De connaître le **nombre d'éléments** dans la file.

    === "Implémentation en `Python`"
        !!! warning "L'implémentation des files en python se fait de manière analogue aux piles"

            - `#!python ma_file.append(ma_valeur)   # permet d'enfiler une valeur`
            - `#!python ma_file.pop(0)               # permet de défiler une valeur`
            - `#!python len(ma_file)                # renvoie la longueur de ma_file`

        === "Implémentation avec `ListeC`"
            !!! example "Implémentation avec une Liste chainée"
                
                !!! bug "Définir en `Python` une classe **`File`**  en utilisant des maillons de liste chaînée"
                    La structure de **liste doublement chaînée** est bien adaptée pour une implémentation de file.  
                    L'idée est d'utiliser une suite de maillons, chaque maillon contenant d'une part une donnée et d'autre part un lien sur le maillon suivant de la chaîne et un lien sur le maillon précédent. 
                    <!-- Le dernier élément de la liste possède un pointeur `M.suiv` vide : ![listech](assets/images/drawit-diagram-17.png)  et le premier un pointeur `M.prec` vide : ![listech](assets/images/drawit-diagram-17.png) -->  

                    ![filec](assets/images/drawit-diagram-23.png)

                    Chaque élément (ou maillon) `M`  de la liste est composé :  

                    - d'un contenu utile `M.val`  (de n'importe quel type),
                    - d'un pointeur `M.suiv` pointant vers l'élément suivant de la séquence,
                    - d'un pointeur `M.prec` pointant vers l'élément précédent de la séquence.
                    
                    ![filech](assets/images/drawit-diagram-46.png)
             
                **Réaliser** l'interface complète de cette file.

                {{IDE('listec/file_avec_listeC')}}

        === "Implémentation avec un Tableau"
            !!! example ""
                !!! bug "Définir en `Python` une classe **`File`** en utilisant une *liste Python* comme attribut"
                    À l'aide de seulement 2 méthodes du type `list` et une fonction
            
                **Réaliser** l'interface complète de cette file dont les méthodes correspondront aux opérations autorisées.

                {{IDE('listec/file_avec_list')}}

                ??? example "Remarque :"
                    - Le type `list` ne permet pas d'implémenter une file en respectant la complexité d'ajout ou de suppression qui doit normalement s'effectuer en temps constant $\mathcal{O}(1)$.  
                      Utiliser deux piles pour implémenter une file remédie au problème de complexité en temps constant.  

                      ![file2p](assets/images/drawit-diagram-39.png){: .center }

                      L'idée est la suivante : on crée un pile d'entrée et une pile de sortie.  
                       
                       ![](assets/images/2piles1file.webp){: .center .width=30%}

                      - quand on veut enfiler, on emplie sur la pile d'entrée  
                      - quand on veut défiler, on dépile la pile de sortie  
                        si celle-ci est vide, on dépile entièrement la pile d'entrée dans la pile de sortie.

                      ```py3 linenums='1'
                      class File:
                        def __init__(self):
                            self.entree = Pile()
                            self.sortie = Pile()

                        def est_vide(self):
                            return self.entree.est_vide() and self.sortie.est_vide()

                        def enfile(self,x):
                            self.entree.empile(x)

                        def defile(self):
                            if self.est_vide():
                                raise IndexError("File vide !")
                            if self.sortie.est_vide() == True :
                                while self.entree.est_vide() == False :
                                    self.sortie.empile(self.entree.depile())
                            return self.sortie.depile()
                      ```

                      ```py3
                        >>> f = File()
                        >>> f.enfile(5)
                        >>> f.enfile(8)
                        >>> f.defile()
                        5
                      ```



                    - La classe `deque` (*double-ended queue*) du module `collections` remédie aussi à ce problème de complexité.  
                      Par exemple dans une console :

                      ```py3
                      >>> from collections import deque
                      >>> queue = deque(["Eric", "John", "Michael"])
                      >>> queue.append("Terry")           		# Terry arrive
                      >>> queue.append("Graham")          		# Graham arrive
                      >>> queue.popleft()                 		# le premier arrivé est enlevé
                       'Eric'
                      >>> queue.popleft()                 		# le deuxième arrivé est enlevé
                       'John'
                      >>> queue                           		# liste dans l'ordre d'arrivée
                        deque([‘Mickael','Terry', ‘Graham'])
                      ```

                    <!--  class collections.deque([iterable,[maxlen]]), renvoie un nouvel objet deque initialisé de gauche à droite (en utilisant append()) avec les données d'iterable. Si iterable n'est pas spécifié, alors la nouvelle deque est vide. Les deques sont une généralisation des piles et des files.  deque se prononce "dèque" et est l'abréviation de l'anglais double-ended queue) : il est possible d'ajouter et retirer des éléments par les deux bouts des deques. Celles-ci gèrent des ajouts et des retraits et sont efficients du point de vue de la mémoire des deux côtés de la deque, avec approximativement la même performance en O(1) dans les deux sens. 
                    Bien que les objets list gèrent des opérations similaires, ils sont optimisés pour des opérations qui ne changent pas la taille de la liste.  
                    Les opérations pop(0) et insert(0, v) qui changent la taille et la position de la représentation des données sous-jacentes entraînent des coûts de déplacement de mémoire en O(n). -->

    === "Exemples d'utilisation"
        !!! example "Les files sont extrèmement utiles en informatique et vous les utilisez quotidiennement, parfois même sans vous en rendre compte :"

            - En général, on utilise des files pour mémoriser temporairement des transactions qui doivent attendre pour être traitées.  
              Une file d'attente agit commme une mémoire tampon (*buffer*) entre une tache et le service qu'elle appelle.
            - Les files dans les files d'impression où le premier document envoyé à l'imprimante sera le premier document à être imprimé.

        ??? example "Prédire/comprendre"
            
            - Une application possible d'une file est l'inversion de l'ordre des éléments d'une pile.  
              En effet, il suffit de dépiler tous les éléments de la pile, puis de les ré-empiler dans l'ordre ou ils sont sortis. On utilisera donc une file comme stockage intermédiaire.
              ```py3
              def inverse_pile(p):
                """
                entrée/sortie p: Tpile
                post-cond: inverse l'ordre des éléments de p
                """
                f = File()
                while not p.estVide() :
                    e = p.lireSommet()
                    p.depiler()
                    f.enfiler(e)
                while not f.estVide():
                    e = f.lireAvant()
                    f.defiler()
                    p.empiler(e)
              ```

            <!-- {{IDE('listec/inv_pile')}} -->
