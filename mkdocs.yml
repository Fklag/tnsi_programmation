site_name: "Terminale NSI - Programmation"
site_url: https://fklag.gitlab.io/tnsi_programmation
repo_url: https://gitlab.com/Fklag/tnsi_programmation
edit_uri: tree/main/docs/
site_description: informatique au Lycée Beaussier - F. Lagrave
copyright: |
  Copyleft &#127279 2021 <a href="https://fklag.gitlab.io/tnsi_programmation" target="_blank" rel="noopener">F. Lagrave</a>
docs_dir: docs


#use_directory_urls: true
#voir https://www.mkdocs.org/user-guide/configuration/#docs_dir

nav:
  - Présentation: index.md
  - Révisions :
    - Les tableaux/listes de Python : tableaux.md
  - Bonnes pratiques:
    - Pratiques de programmation : pratiques.md
  - Récursivité : recursivite.md
  - Paradigmes:
    - Notion_de_Paradigmes : Notion_de_Paradigmes-Introduction.ipynb
    - Paradigme de Programmation : paradigme.md
  - POO :
    #- Programmation Orientée Objet : POO-IntroductionProgrammationOrienteeObjet.ipynb
    - Notions de programmation orienté objet : poo.md
  - Automatismes: 
    #- Première: automatismes/premiere/automatismes_premiere.md
    - Maths complémentaires: automatismes/maths_complementaires/automatismes_maths_complementaires.md
  - Oral: 
    - Sujets d'oral en première: premiere/sujets_oral.md
    - Sujets d'oral en terminale: maths_comp/sujets_oral.md
    - Grand oral: grand_oral/grand_oral.md
  #- Enigmes: enigmes/enigmes.md
  
    
    
    



theme:
  name: material
  palette:                        # Palettes de couleurs jour/nuit
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      primary: blue
      accent: blue
      toggle:
          icon: material/weather-night
          name: Passer au mode jour
    - media: "(prefers-color-scheme: light)"
      scheme: default
      primary: indigo
      accent: indigo
      toggle:
          icon: material/weather-sunny
          name: Passer au mode nuit
  custom_dir: overrides
  font: false
  language: fr
  logo: assets/images/illu.png
  favicon: assets/images/favicon-12.png
  features:
    - navigation.instant
    - navigation.tabs
    - navigation.expand
    - navigation.top
    - toc.integrate
    - header.autohide
    - navigation.tabs.sticky
    - navigation.sections


markdown_extensions:
  - meta
  - abbr
  - def_list     # Les listes de définition.
  - attr_list    # Un peu de CSS et des attributs HTML.
  - admonition   # Blocs colorés  !!! info "ma remarque"
  - pymdownx.details  #   qui peuvent se plier/déplier.  
  - footnotes    # Notes[^1] de bas de page.  [^1]: ma note.
  - pymdownx.caret  # Passage ^^souligné^^ ou en ^exposant^.
  - pymdownx.mark  # Passage ==surligné==.
  - pymdownx.tilde   # Passage ~~barré~~ ou en ~indice~.
  - pymdownx.snippets   # Inclusion de fichiers externe.
  - pymdownx.highlight:  # Coloration syntaxique du code
      #linenums: true
  - pymdownx.tasklist:  # Cases à cocher  - [ ]  et - [x]
      custom_checkbox:    false   #   avec cases d'origine
      clickable_checkbox: true    #   et cliquables.
  - pymdownx.inlinehilite # pour  `#!python  <python en ligne>`
  - pymdownx.superfences: # Imbrication de blocs.
      preserve_tabs: true
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_div_format        
  - pymdownx.keys   # Touches du clavier.  ++ctrl+d++
  - pymdownx.tabbed
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.emoji:   # Émojis  :boom:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
  - pymdownx.arithmatex:
      generic: true
      smart_dollar: false
  - toc:
      permalink: ⚓︎
      toc_depth: 3
  - mkdocs_graphviz  #https://gitlab.com/rodrigo.schwencke/mkdocs-graphviz


plugins:
  - search
  - kroki
  - macros:
      verbose: True
      include_dir: include # voir https://mkdocs-macros-plugin.readthedocs.io/en/latest/advanced/#changing-the-directory-of-the-includes
  - mermaid2
  - mkdocs-jupyter: # Convertir les fichiers .ipynb et .py en pages du site https://github.com/danielfrg/mkdocs-jupyter
      kernel_name: python3
      execute: False
      include_source: True

  

extra:
  social:
    - icon: fontawesome/solid/paper-plane
      link: mailto:lycee.lagrave@free.fr
      name: Mail

    #- icon: fontawesome/brands/gitlab
    #  link: https://www.gitlab.com/Fklag/tnsi_programmation/
    #  name: Dépôt git


    - icon: mattermost/mattermost
      link: http://lycee.lagrave.free.fr
      name: F. Lagrave

    - icon: fontawesome/solid/university
      link: https://www.atrium-sud.fr
      name: Lycée Beaussier

  
  site_url: https://fklag.gitlab.io/tnsi_programmation/
  



copyright:  Sous licence CC BY-NC-SA 4.0

extra_javascript:
  - javascripts/config.js
  #- javascripts/pyodid.js
  #- javascripts/interpreter.js
  - xtra/javascripts/interpreter.js
  #- javascripts/repl.js
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
  #- https://cdn.jsdelivr.net/pyodide/v0.17.0/full/pyodide.js    #chargé dans l'extension du template html overrides/main.html

extra_css:
  - stylesheets/extra.css
  - stylesheets/fenarius.css
  - xtra/stylesheets/pyoditeur.css
 
