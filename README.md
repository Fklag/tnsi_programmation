# TNSI

<quote>Dépôt pour le site "TNSI_programmation" du lycée Beaussier généré avec Mkdocs</quote>

 <article>
          <p>Tous les documents sur ce site sont   placés sous licence 
            <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/"> 
              Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0).</a>
              </p>  
          <p>Sources :  <a href="https://github.com/parc-nsi">parc-nsi</a> F-Junier + <a href="https://gitlab.com/ferney-nsi/">ferney-nsi</a> V-Bouillot 
         
 </article>
